\section[Computational infrastructures]{Computational infrastructures and the need for high performance computing}
\label{sec:hardware}
Three dimensional general relativistic simulations, like the ones presented in this thesis, require large computational resources. At each time-step, the code evolves in time 17 spacetime variables (including gauge variables) and 5 hydrodynamical variables (including their reconstruction, flux computation, and conservative to primitive conversion, which are all time-consuming algorithms). This is done for every point in the computational domain, which, at our standard resolution ($dx = \SI{0.25}{\msun}$) and grid configuration (see table \ref{tab:grid}) is made by 6 grids of around $\frac{180^3}{2}$ points each, where the division comes from bitant symmetry. In practice, the inner grids are smaller, also because they are not perfect cubes but have less points in the $z$ direction. To this gridpoint number, one must add the buffer points needed for Runge-Kutta integration (see sec. \ref{sec:curvature}) around the refinement boundaries. Table \ref{tab:cost} shows the computational cost of (relatively short) \SI{30}{ms} simulation at different spatial resolution using the optimal run parameters on the Galileo machine. In the case of our standard resolution, a run of x days on 256 cores is needed.

\begin{table}
 \centering
 \begin{tabular}{rcccccc}
  Level & min($x$/$y$)& max($x$/$y$)& min($z$)& max($z$)& $(N_x,N_y,N_z)$ \\
        &  (CU)       &     (CU)    &   (CU)  &  (CU)   & ${dx=0.25}$\\
\hline
  $1$ & $-720$ & $720$ & $0$ & $720$  & (185,185,96)\\
  $2$ & $-360$ & $360$ & $0$ & $360$  & (205,205,106)\\
  $3$ & $-180$ & $180$ & $0$ & $180$  & (205,205,106)\\
  $4$ & $ -90$ & $ 90$ & $0$ & $ 90$  & (205,205,106)\\
  $5$ & $ -60$ & $ 60$ & $0$ & $ 30$  & (265,265,76)\\
  $6$ & $ -30$ & $ 30$ & $0$ & $ 15$  & (265,265,76)\\
 ($7$ & $ -15$ & $ 15$ & $0$ & $7.5$) & (265,265,76)\\
 \end{tabular}
 \caption{Simulation grid boundaries of refinement levels. Level 7 is
          only used for simulations forming a BH, once the minimum
          of the lapse $\alpha < 0.5$.}
 \label{tab:grid}
\end{table}

\begin{table} 
\begin{tabular}{|l|rrrrrr|}  
\hline
$\bigtriangleup x$ (CU) & $0.75$ & $ 0.50$ & $0.375$ & $0.25$ & $0.185$& $0.125$  \\   
\hline  
\# threads               &   16 &   64 &  128 &  256 &  512 & 2048 \\
\# MPI                   &    2 &    8 &   16 &   32 &   64 &  256 \\
\hline
Memory (GBytes)          &  3.8 &   19 &   40 &  108 & 237  &   768 \\
%%                       &  1.9 &  2.3 &  2.5 &  3.4 &  3.7 &   3.0 \\
speed (CU/h)             &  252 &  160 &  124 &   53 &   36 &    16 \\ 
speed (ms/h)             & 1.24 & 0.78 & 0.61 & 0.26 & 0.18 & 0.08  \\ 
cost (SU/ms)             &   13 &   81 &  209 &  974 & 2915 & 26053 \\\hline
total cost (kSU, $30$ ms) & 0.39 &    3.6 & 6.3 &   39 &  88 & 780  \\
\hline
\end{tabular}
\caption{Computational cost of the simulations, for the example of using
BSSN-NOK, with WENO reconstruction for the hydrodynamics. SU stands for
service unit: one hour on one CPU core.
The reported
values refers to the ``GALILEO'' PRACE-Tier1 machine locate at CINECA 
(Bologna, Italy) equipped with 521 nodes, two-8 cores Haswell 2.40 GHz,
with 128 GBytes/node memory and 4xQDR Infiniband interconnect.
Also, these are only correct for evolutions that 
do not end with the formation of a BH, as an additional refinement
level was used to resolve the BH surroundings, and more analysis
quantities had to be computed (e.g., the apparent horizon had to be found).
In addition, the simulations resulting in a BH were performed on facilities
at Louisiana State University: SuperMike II (LSU HPC) and QB2 (Loni).}
\label{tab:cost}
\end{table}

Efficient parallel programming is essential for the code to be able to make out the most of the multi-cores and multi-nodes clusters we had allocations on. The code parallelism is handled in the Einstein Toolkit by the \codename{Carpet} thorn. It implements an hybrid MPI-OpenMP parallelization. With MPI, the computational domain of each refinement level is divided among the requested processes, each running on one available core. Each process must evolve also some points at the boundary of its domain, called \textit{ghost points}, which are evolved also by processes responsible for the adjacent domains. There are three ghost points in every direction, as the evolution finite differencing and reconstruction algorithms employed need a three points stencil. At each Runge-Kutta step in the evolution, just after applying the boundary conditions, the ghost points are synchronized between all processes, via MPI communications. There communications take time which is subtracted from the computation time, so they should be reduced to a minimum. One way to try to do so, implemented in Carpet, is to \quotes{overlap computations and communications}, using non-blocking MPI commands for exchanging messages between processes. OpenMP parallelization, instead, distributes the code computations inside loops to multiple threads, which are then reabsorbed in one single master thread after the loop's end. Most of the Einstein Toolkit is OpenMP parallelized, but some part of the code is inevitably not written in loops over the grid points, so should be executed in a serial manner. 

Our production runs have been performed mainly in three similar machines, Cineca's Galileo cluster, thanks to the agreement between Cineca and INFN (specific initiative \quotes{Teongrav}), and on Louisiana State University clusters Mike and QueenBee, thanks to the allocations [...]. Galileo is a tier-1 cluster composed by 512 nodes, each mounting two eight-cores Intel Haswell processors and 128 GB of ram. The nodes are linked with an Infiniband network. Some nodes have also Nvidia K80 GPUs or Intel Phi 7120p accelerators, but the Einstein Toolikit is not yet able to take full advantage of them. QueenBee is as cluster at LSU made by 680 compute nodes, each mounting two quad-core Intel Xeon processors and 8GB of ram. Finally, the SuperMike cluster, also hosted at LSU, and now substituted by SuperMikeII, had 512 compute nodes with dual core Pentium IV Xeon processors and 2GB of ram each.

Figure \ref{fig:strong_scaling} shows the scaling features of our code on the Galileo cluster, representing, in the upper panel, the evolution time (in solar masses) computed every hour of code run, using a different number of physical cores, MPI processes and OpenMP threads for each process.  
\begin{figure}
\begin{centering}
  \includegraphics[width=.8\hsize]{figs/SpeedPlotGalileo_thesis.pdf}\\
\end{centering}
\vspace{-2mm}
\caption{Strong scaling performance of the Einstein Toolkit with our parameters, for a binary neutron star simulation, using different resolutions, number of physical cores and nodes, and number of threads for each MPI process. The best setup requires using 8 OpenMP threads each process. All the other thread number (NT) possibilities are shown for resolution $dx=\SI{0.75}{\msun}$ only for simplicity. These test runs are performed on Cineca's Galileo machines, on Intel processors. Each node has two processors with 8 cores each.}
\label{fig:strong_scaling}
\end{figure}
The ideal scaling (dashed lines) is obviously not reached, since there are many factors limiting the actual code scaling, such as the presence of some serial parts in the code (as the output of 1D and 2D data, while the 3D data output is split among all MPI processes), which, following Ahmdal's low, set a limit to the overall maximum scaling, or the use of buffer zones or other kinds of grid points not really increasing the spatial resolution, or the MPI communication, which becomes more important increasing the number of processes requested. Nevertheless, the parallel efficiency, shown in the lower panel, is quite satisfactory for this kind of grid-based code, allowing to use a large number of cores without wasting too much computational resources for one's allocation. Such a good scaling could not be reproduced on more \textit{exotic} computer architectures, such as the Fermi Blue Gene Q machine, also hosted at Cineca. When performing such kind of numerical simulations, we are not really interested in getting close to strong scaling, but instead we run in the so called week scaling regime. Week scaling means keeping constant execution time increasing the size of the computational problem and the number of used cores by the same amount. Running a simulation in a week scaling regime means using the least number of processors needed for providing the necessary memory for the application to run. This is the best strategy to spend efficiently the computing time allocation, especially for memory consumption intensive applications. 