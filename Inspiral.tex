\section{Inspiral gravitational waveform}
\label{sec:inspiral}
Figure \ref{fig:h} already showed some of the main characteristics of the GW emission in the coalescent phase, before merger, and their dependence on different parameters, such as the stars mass, mass ratio and EOS. In order to quantitatively evaluate those differences, and, in particular, the one due to the EOS, about which we are interesting to gather information from GW detections data, all the error sources for the simulated signal should be under control \cite{Hinder2013,Hotokezaka2016a}. The problem of the GW signal extraction from simulations was already addressed in section \ref{sec:extraction}, while the evolution code error and its convergence properties will be discussed extensively in appendix \ref{sec:convergence}. In this section, instead, I will analyse two other often ignored sources of errors, which, nevertheless, could be dominant and hide the true signal features due to tidal effects. The first error source is the eccentricity of the evolved orbits. As reported in table \ref{tab:dns}, all the observed BNS systems are believed to have a negligible eccentricity when they will enter in the LIGO/Virgo sensitivity band, due to the circularizing effect of GW emission. On the other hand, from the signals in figure \ref{fig:h}, and in particular the longer ones in the bottom-right panel, one can clearly see by eye the presence of amplitude oscillations with a fixed frequency. In the next subsection I will prove that they are not caused by GW extraction errors, but they are the imprint of a physical process, the eccentricity of the stars orbits.
Another potentially important error source is linked with initial data, which are produced taking some approximations, like conformal flatness (see sec. \ref{sec:initial_data}). These possible errors could be evaluated by comparing simulation of the same model with initial data generated by different codes, as done in ref. \cite{Tsokaros2016}. Unfortunately, only one BNS initial data code is publicly available (the LORENE library), and so such comparison is difficult to reproduce. Instead, a different, interesting technique is to compare the results of simulations starting with a different initial interbinary distance (which means, a different initial orbital and GW frequency). This allows, also, to study which initial distance is the best for performing numerical BNS simulations, depending on which phase of the binary evolution one is most interested in studying.  

\subsection{Residual eccentricity}
\label{sec:e}
The presence of a residual orbital eccentricity in BNS numerical evolution is inevitable, if one imposes an Helical Killing vector in the initial data generation. It is a consequence of the resulting binary configuration having no radial velocity component, which in reality would be present due to the GW radiation reaction. The eccentricity can be measured from the stars orbits, and then the GW signals coming from the simulations can be compared with the ones predicted by post-Newtonian approximations of eccentric binaries. If they match, then we can confirm that the amplitude oscillations are indeed due to the eccentricity, and quantify how much they influence the accuracy of the simulated GW signals.
%\begin{figure}
%	\begin{centering}
%	\includegraphics[width=0.8\textwidth]{figs/Orbits_thesis.pdf}
%	\end{centering}
%\vspace{-0.8mm}
%\caption{Orbits of one of the stars in an equal mass BNS model simulation, with mass of $\SI{1.4}{\msun}$, using different EOS, and starting from an initial distance of \SI{50}{km}. The line in the X-Y plane traces the position of the star center, defined as the point with maximum density.}
%\label{fig:traj} 
%\end{figure}
The simplest way to compute the trajectory of a binary is to follow the stars centres, which can be defined as the points with maximum density (or minimum lapse $\alpha$, given our singularity-avoiding gauge choice). A possible improvement on this technique, suggested also in ref. \cite{Kyutoku2014}, is to compute the stars centres with a volume average, similar to a Newtonian center of mass computation:
\begin{equation}
x^i = \frac{\int_{star}{\rho x^i d^3x}}{\int_{star}{\rho d^3x}}.
\end{equation} 
To estimate the orbital eccentricity, a common procedure (see, for example, ref. \cite{Kyutoku2014,Dietrich2015a}) is to rely on the following Newtonian parametrization for the time derivative of the distance between the stars centres:
\begin{equation}
\dot{D}(t)\ =\ A_0 + A_1 t - e D_0 \omega_e sin\rnd{\omega_e t + \phi_e},\label{eq:ecc}
\end{equation} 
where $e$ is the eccentricity, $D_0 = d$ is the initial distance and $\omega_e$ is the angular frequency of the eccentricity oscillations. If one computes the distance between the star centres from the simulation 3d output, it is possible to use eq. \ref{eq:ecc} to obtain the eccentricity value with a non-linear fit. The simplest, although incorrect, way of computing the distance is simply to take the coordinate distance between the centres. A better way would be to take, instead, the proper distance along a straight line between them. Finally, the correct procedure would be to compute the geodesic which passes through the two stars centres, and compute the proper distance along it. This will be, however, computationally expensive, because it should be repeated for every time-step for which one has 3d outputs. Therefore, in the following analysis, I adopted the simplest procedure, and verified afterwards its adequacy confronting the obtained eccentricity value with the one coming from the GW signal amplitude oscillations. It was chosen to fit the derivative of $D$ and not $D$ itself, because having one less free parameter brings an advantage greater than the error made by the numerical derivative (which is performed using a fourth-order finite differencing operator). It was also tried to fit the evolution of the GW frequency, but the result was found to be much more noisy. The fit is performed starting from $t_{ret} = \SI{3}{ms}$ (to avoid the spurious radiation emitted at the simulation beginning, see next paragraph) and $t_{ret} = \frac{2}{3} t_{merger}$ (to avoid the plunge phase, in which the stars come into contact and merge, and eq. \ref{eq:ecc} is no more a good representation for the distance derivative evolution).
\begin{figure}
	\begin{centering}
	\includegraphics[]{figs/AllSimsOrbit.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Evolution of the distance between the star centres, for equal mass BNS models with different EOS and initial separation. The oscillations in the distance are due to the residual orbital eccentricity, and are larger in the softer models.}
\label{fig:D} 
\end{figure}

\begin{figure}
	\begin{centering}
	\includegraphics[]{figs/AllSimsPN_thesis.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Evolution of the time derivative of the distance between the star centres, for equal mass BNS models with different EOS and initial separation. The oscillations in the distance derivative are due to the residual orbital eccentricity, and are larger in the softer models. The blue line represents actual data taken from the simulation output, while the red line is the result of the fit with eq. \ref{eq:ecc}.}
\label{fig:e_fit} 
\end{figure}
The resulting evolution of the stars separation, for models with different EOS and initial distance, is shown in figure \ref{fig:D}. In all the simulations is visible an initial sudden drop in the distance. This is due to an initial burst of unphysical spurious gravitational radiation (visible also from the signals in figure \ref{fig:h}), which is due to a gauge readjustment because the initial spacetime did not have the right radiation content, which physically it should have due to the GW emitted by the binary up to that point of stars separation. This is a consequence of the \textit{conformal flatness} approximation made in the initial data computation.

The subsequent evolution is again similar in all models, with the distance more rapidly decreasing towards the merger (when the GW emission has a higher frequency and amplitude), and showing characteristic oscillations, typical of eccentric orbits. It is interesting to note that the evolution of models starting from a smaller initial separation is slightly different than the models starting from $d = \SI{60}{km}$, with a faster orbital shrinking, in particular for the more compact stars (with APR4 and SLy EOSs). More on this phenomenon can be found in the next subsection.

The fitting curves are represented in red in figure \ref{fig:e_fit}, where the blue curves are the original distance data, instead. It is clear that at least one eccentricity cycle, before the plunge phase, when the parametrization of eq. \ref{eq:ecc} is probably not a good approximation anymore, is necessary for correctly estimating the orbital eccentricity. Therefore, the $d = \SI{40}{km}$ simulations will be excluded from the following analysis, and the results for the $d = \SI{44}{km}$ simulations with the stiffer EOS should be taken with care. 

\begin{table}
\begin{tabular}{|c|ccc||c|c|}
\hline
& e & e &e &  $\|h_{T4} - h_{eT4}\|$ & R [Mpc]\\
EOS  & $d=60$km& $d=50$km&$d=44.3$km & $R=100$Mpc & $\|h_{T4} - h_{eT4}\|=1$ \\
\hline
APR4 & 0.028 & 0.020 & 0.020 & 0.67 & 67\\
\hline
SLy & 0.025 & 0.019 & 0.020 & 0.58 & 58\\
\hline
H4 & 0.012 & 0.012 & 0.014 & 0.33 & 33\\
\hline
MS1 & 0.014 & 0.014 & 0.007 & 0.35 & 35\\
\hline
\end{tabular}
\caption{Results of the orbital eccentricity analysis. The first three columns show the eccentricity
  parameters obtained fitting equation \ref{eq:ecc}. The fourth column
  shows the detectability of the eccentricity in a TaylorT4
  approximate waveform with the same initial parameters of the model
  with the corresponding EOS and $d=60$~km, for an optimally aligned
  binary at $100$~Mpc. The fifth column shows the maximum optimally
  aligned binary distance for the eccentricity effect to be marginally
  detectable, calculated as $\|h_{T4} - h_{eT4}\|_{100Mpc}\times
  100$~Mpc.}
\label{tab:e}
\end{table}

The resulting eccentricity values are reported in table \ref{tab:e}. They are higher for the more compact stars, of about a factor 2.
\begin{figure}
	\begin{centering}
	\includegraphics[]{figs/eccentricity_d44_d50.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Gravitational wave strain amplitude for the same models of fig. \ref{fig:e_fit}. The black line represents the numerical simulation data, while the red line is a approximated post-Newtonian waveform for point-particles in an eccentric orbits (see ref. \cite{Tanay2016}).}
\label{fig:PNecc} 
\end{figure}

To assess the correctness of the eccentricity computation procedure, and to evaluate if the GW amplitude oscillations are really an imprint of the orbital eccentricity, it is possible to compare the GW signal extracted from numerical simulations with one generated with post-Newtonian techniques for eccentric binaries. Recently, different groups have proposed techniques to generate analytical approximations of the inspiral waveforms for point particles in eccentric orbits \cite{Tanay2016,Moore2016,Loutrel2016}. These should be close to the BNS GW signal from numerical simulations, far from the merger phase, when tidal effects are not important. For this reason, many orbits simulations, with a large initial separation between the stars, are necessary to carry on this analysis. Figure \ref{fig:PNecc} shows the GW amplitude for the BNS models of fig. \ref{fig:D} (black lines) and corresponding PN waveforms, generated following ref. \cite{Tanay2016} (accurate up to 2PN order in phase and Newtonian order in amplitude), and using the public LIGO Algorithm Library (LAL), with the same initial gravitational mass, eccentricity and frequency of the simulated model. At the beginning of the simulations, especially for the stiffer models, the PN approximant perfectly reproduces the simulated GW strain amplitude, including its oscillations. After a while, the simulated signal departs from the analytical one, because tidal effects and missing high-order post-Newtonian terms start to be important, but it keeps the same oscillations structure, until the final plunge phase. The difference between GWs from BNS simulations and point-particle PN approximations becomes farther from the merger in the most compact stars, in particular the ones with the APR4 EOS. This perfect match is less recognizable in the $d=\SI{44}{km}$ models, which are close to the plunge phase already at the beginning of the simulations, and which have only a few eccentricity cycles (one for the H4 and MS1 models) before the merger. These results confirm that GW amplitude oscillations, remaining after our careful extraction procedure, are entirely due to the orbital eccentricity. I want to stress here, looking again at figure \ref{fig:h_abs}, that the use of the digital filtering after $\Psi_4$ integration is necessary for this comparison with the eccentric PN waveforms. With all the other strain amplitudes
in fig. \ref{fig:h_abs} computed with different integration methods, it would have been impossible to draw any conclusion from an analysis like the one in figure \ref{fig:PNecc}, due to the presence of a complex amplitude oscillations structure (given by noise and numerical integration errors), among which one cannot distinguish the physical oscillations caused by the orbital eccentricity. 
\begin{figure}
	\begin{centering}
	\includegraphics[]{figs/AllSimsPNPhase.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Difference in phase between a circular-orbits TaylorT4 approximant and a eccentric orbit post-Newtonian waveform, at the same PN order, starting from the same gravitational mass, frequency and eccentricity.}
\label{fig:PNeccPhase} 
\end{figure}

It is interesting to assess which would be the impact of the orbital eccentricity introduced by quasi-circular initial data on GW detection and the following source parameters estimation. A possible first step in that direction is looking at the difference in phase between an eccentric post-Newtonian approximant, with the same gravitational mass and eccentricity of each of the simulated models in figure \ref{fig:D} with $d=\SI{60}{km}$, and a standard TaylorT4 analytical waveform for point-particles coalescing in circular orbits, with the same masses. The result can be seen in figure \ref{fig:PNeccPhase}. For the less compact models (with H4 and MS1 EOS, and an eccentricity around 0.01), the phase difference between the eccentric and the circular waveform oscillates around zero, with a maximum amplitude of about $0.1 rad$, which is most probably lower than the numerical errors of the evolution code (a lower bound of which is estimated in the next section). In the plunge phase, this difference raises up to $0.4 rad$. For the more compact models (with the APR4 and SLy EOSs, and an eccentricity between 0.02 and 0.03), instead, the phase difference oscillates around zero with an amplitude of $0.2 rad$ for most of the inspiral, and can reach values close to a radiant at merger. This means that, while probably negligible in the less compact models, initial data eccentricity is a major source of error in numerical simulations of binary neutron stars inspiral for stars with a soft EOS. From figure \ref{fig:PNecc}, we already noted that eccentricity oscillations are the dominant factor in the difference between gravitational waveforms from many orbits BNS simulations (black lines) and point-particle post-Newtonian approximations (green lines), until the plunge phase, , even at low post-Newtonian order. This can make the assessment of tidal effects (and then EOS) impact on the gravitational signal more difficult, and hence can harm the efforts to pinpoint the neutron stars EOS from GW detections. 

Another interesting evaluation is to measure the detectability, in the Advanced LIGO/Virgo interferometers, of the difference between eccentric and circular orbits waveforms. In particular, performing this computation starting from the initial frequency of the $d=\SI{60}{km}$ simulations presented before, which are among the longest (in the inspiral phase) BNS simulations performed so far, allows to understand the importance of initial data eccentricity error in currently feasible BNS simulations, even if a detectability comparison should, in theory, be computed using a signal covering all the detectors frequency band. To do so, one can compute the distinguishability of two waveforms $h_1$ and $h_2$, using the following expression \cite{Lindblom2008,Read2013,Hotokezaka2016a}:
\begin{equation}
||h_1 - h_2|| = min_{\Delta t, \Delta \phi} \sqrt{4\int_{f_0}^{f_1}{\frac{\abs{\hat{h}_{+,1}(f) - \hat{h}_{+,2}(f)e^{i(\rnd{2\pi f\Delta t+\Delta \phi}}}^2}{S_n(f)}}},
\end{equation}
where $\hat{h}_{+,i}(f)$ are the Fourier transform of the plus polarization of the two compared strains, $f_0 = \SI{9}{hz}$ and $f_1 = \SI{7000}{hz}$ are, approximatively, the limits of the Advanced LIGO frequency band, and $S_n(f)$ is the one sided noise power spectral density for Advanced LIGO, in the \textit{zero detuning, high power} configuration, which will be the best for detecting binary neutron stars mergers. Using this norm, it was shown in \cite{Lindblom2008} that two detected gravitational waveforms will be distinguishable if $||h_1 - h_2|| \geq 1$. In particular, in the limit $||h_1 - h_2|| = 1$, the marginally distinguishable case, the two waveforms can be distinguished with a $1\sigma$ statistical significance. 

In table \ref{tab:e} is reported the distinguishability between circular and eccentric orbits waveforms, computed for optimally aligned binaries at a distance of $R = 100Mpc$, and the corresponding minimum distance to have marginal distinguishability. The detectability of eccentricity effects at \SI{100}{Mpc} is small, mainly because of the shortness of current numerical waveforms respect to the whole LIGO frequency band. When longer simulations will be available, especially to build hybrid analytical-numerical waveforms, able to cover the whole detectors band, this eccentricity effect will become more important.

Given these results, it is clear the importance of getting rid of the initial eccentricity of BNS orbits, in order to evaluate the difference between different EOSs and to model accurately tidal effects and other possible effects like stellar oscillations present in the inspiral phase. Work in this direction, mimicking what is already standard for binary black holes simulations, has been done by some groups \cite{Kyutoku2014,Dietrich2015a}. In particular, they use an iterative procedure, first evolving eccentric initial data, like LORENE's ones, and computing the eccentricity with eq. \ref{eq:ecc}. Then, they correct accordingly the initial data, adding a radial velocity component, and start again the simulation. This process is repeated (usually three times), until a target value for the eccentricity is reached. For example, for BBH simulations, the NRAR (Numerical Relativity and Analytical Relativity) collaborations prescribed $e < \num{2e-3}$, which is an order of magnitude lower than the one of LORENE's models. However, there is not yet a publicly available tool to generate low-eccentricity BNS initial data, which can be checked and evolved by anyone in the scientific community. I plan to dedicate some of my future work, immediately after this thesis, to create such tool.  

\subsection{Effect of the initial interbinary distance}
\label{sec:d}
Binary neutron star merger simulations presented in the literature start from a variety of initial values for the stars centres separation. As seen at the beginning of this section, long, many orbits, BNS simulations are necessary to perform comparisons with post-Newtonian approximations (either to construct longer hybrid analytical-numerical waveforms, to cover all the current GW detectors frequency band, or to evaluate the accuracy of different PN formulations and calibrate their free parameters), and to study tidal (and, more in general, EOS) effects on the inspiral waveform. On the other hand, when focusing on the post-merger signal, which is the primary target for three dimensional numerical investigations, for the lack of any analytical treatment so far, one wants to simulate the shortest possible inspiral, not to waste too many computational resources on that phase. From these necessities, several questions arise, such as:
\begin{enumerate}
\item How the numerical errors accumulate during many orbits evolutions? Which is the maximum number of orbits one can simulate with current numerical codes in order to keep a target accuracy?
\item How close to the merger a simulation can start, without propagating errors which can influence the post-merger results?
\item How accurate are the conformal thin sandwich initial data, depending on the stellar separation?
\item What is the difference between tidal deformations and stellar mode excitations developed during a numerical evolution starting from an initial large interbinary distance, or included in close-binaries initial data?
\end{enumerate}
In order to answer all those questions, a first step is to compare simulations of the same model (in the case presented here, an equal mass model with a baryonic mass of \SI{1.4}{\msun} for each star, and the four EOSs presented in sec. \ref{sec:eos}) starting from initial data with a different interbinary distance (which is equivalent to say that they have a different initial orbital frequency). The initial separations considered here are $60, 50, 44.3$ and $40$ km. I can anticipate here that to give a complete answer to the previous questions, this work is not sufficient, and further explorations, using different numerical methods, and techniques to try to disentangle those errors sources, which are difficult to distinguish, are needed. This comparison can also be seen as using the full numerical 3D simulations to fill the gaps between several quasi-equilibrium configurations through which the binary must pass, evaluating with which accuracy are current numerical algorithms able to do so.
\begin{figure}
	\begin{centering}
	\includegraphics[]{figs/h.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Gravitational wave strain plus polarization from simulations of an equal mass binary with $M = \SI{1.4}{\msun}$ for each star, with different EOS and initial interbinary distance.}
\label{fig:h_d} 
\end{figure}
Figure \ref{fig:h_d} gives a first overview of the difference between simulations with different initial stars separation. At the beginning of the shorter simulations, after the first \quotes{junk radiation} emission (explained in sec. \ref{sec:e}), the signals of models with the same EOS agree very well, confirming that the dynamical evolution is able to connect LORENE's generated quasi-equilibrium configurations. However, in the last part of the coalescent phase, after the start of the $d = \SI{40}{km}$ simulations, the signals start to diverge, with simulations starting from a closer interbinary distance having a faster phase evolution (see small boxes in figure \ref{fig:h_d}). It is also interesting to note, contrary to what can be expected a priori, that the initial interbinary distance has also an impact on the GW amplitude in the post-merger phase. This could be potentially very important for numerical studies of the post-merger energy balance. A more quantitative analysis of these discrepancies can be found in section \ref{sec:PM_d}.

In order to investigate the differences in the phase evolution of simulations starting from different separations, during the inspiral, it is useful to align the simulations (with the same EOS) starting from $d = 50, 44.3, 40$ km with the longer ones, starting from $\SI{60}{km}$. At first, the waveform are aligned at the time of the merger, considering them respect to the new time variable
\begin{equation}
\tilde{t} = t_{ret} - t_{merger}(d),
\end{equation}
as they have already been shown in figure \ref{fig:h_d}. Then, following a standard procedure, often used to compare waveforms of the same model generated with different techniques (see, for example, ref. \cite{Boyle2008a,Baiotti2011,Bernuzzi2012}), the signals are aligned finding the time shift $\Delta t$ and the phase shift $\Delta \Phi$, after defining the aligned GW phase
\begin{equation}
\Phi_{al1}(t) = \Phi_d(t-\Delta t) + \Delta\Phi - \Phi_{d=60}(t),\label{eq:al1}
\end{equation}
and then minimizing the following integral:
\begin{equation}
I(\Delta t, \Delta\Phi)\ =\ \int_{t_1}^{t_2}{\abs{\Phi_{al1}(\tilde{t})}^2}d\tilde{t},\label{eq:al1min}
\end{equation}
where the GW phase of the simulation starting from an initial interbinary separation $d$ is defined from the gravitational wave strain $h$:
\begin{equation}
\Phi = arctan\rnd{\frac{h_X}{h_+}}.\label{eq:phi_def}
\end{equation}
The integration limits are $t_1 = \SI{3}{ms}$ and $t_2 = min(\SI{20}{ms},t_{merger}-\SI{2}{ms})$, in order to avoid, as usual, the initial spurious radiation and the plunge phase.
\begin{figure}
	\begin{centering}
	\includegraphics[]{figs/phase_shift.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Difference in the accumulated phase between simulations starting from $d = \SI{60}{km}$ separation and simulations starting from closer configurations, with the same EOS and star masses, aligned with eq. \ref{eq:al1}, allowing for a phase and time shift.}
\label{fig:al1} 
\end{figure}
\begin{figure}
	\begin{centering}
	\includegraphics[]{figs/phase_dilat.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Difference in the accumulated phase between simulations starting from $d = \SI{60}{km}$ separation and simulations starting from closer configurations, with the same EOS and star masses, aligned with eq. \ref{eq:al2}, allowing for a time dilation.}
\label{fig:al2} 
\end{figure}

In figure \ref{fig:al1} are reported the differences between the aligned waves phase and the phase of the $d=\SI{60}{km}$ simulations waveforms. Outside the alignment interval, the phase evolutions between simulations starting from a different orbital frequency diverge quite significantly. This is more pronounced in the more compact stars, with the SLy and APR4 EOSs, showing a phase difference at merger of about 4 radiant. The stars with the H4 EOS have an intermediate behaviour, while the stars with the MS1 EOS show a much lower deviation, with a maximum phase difference at merger of 1 rad.

These results are not really satisfactory, showing a huge difference caused by the initial data separation, which can be mitigated by the alignement procedure in the alignement interval only. This last consideration leads to believe that eq. \ref{eq:al1} is not the best for the signals scrutinized here. A different procedure to test, which can also help identifying the source of such huge phase discrepancies, is allowing for a dilatation of the time variable, as done in ref. \cite{Hotokezaka2013b,Hotokezaka2015} to align waveforms of simulations with different resolutions, in order to compare them compensating for their different merger times. For each simulation with $d < \SI{60}{km}$, one has to find the parameters $\eta$ and $\Delta \Phi_2$ which, after defining
\begin{equation}
\Phi_{al2} (t)\ =\ \Phi_d(\eta \cdot t) + \Delta\Phi_2 - \Phi_{d=60}(t) \label{eq:al2}
\end{equation}
minimize the following integral:
\begin{equation}
I(\eta, \Delta\Phi)\ =\ \int_{t_1}^{t_2}{\abs{\Phi_{al2}(\tilde{t})}^2}d\tilde{t} \label{eq:al2min}.
\end{equation}
The result are presented in figure \ref{fig:al2}, again taking the phase differences between the aligned waveforms and the $d = \SI{60}{km}$ ones. With this different alignment, the phase differences are much smaller than before, and, more importantly, do not increase only outside the alignment interval. All the obtained values for the time dilation factor $\eta$ are greater than one. This can be interpreted as the simulations starting from a closer initial binary configuration carry out more orbits compared with the ones starting from further apart, when comparing them over the same distance before merger. While this is always true comparing the simulations starting from $d = \SI{60}{km}$ with the ones starting closer, the effective merger times of simulations starting from 50, 44.3 and 40 km, instead, do not follow always the same trend, suggesting the presence of other sources of error, especially for the smallest initial distances.

\begin{table}
\begin{centering}
\begin{tabular}{|c||cc||ccc|}
\hline
\multirow{2}{*}{Model} & \multicolumn{2}{c||}{Shift alignment (Eq.~\ref{eq:al1})} & \multicolumn{3}{c|}{Dilatation alignment (Eq.~\ref{eq:al2})}\\
\cline{2-6}
& $\Delta t$ (ms)& $\Delta\phi$ (rad)& $\eta$ & $\Delta\phi_2$ (rad)& $\Delta t_\mathrm{merger}$ (ms)\\
\hline
APR4(d=40 km) & 1.40 & 6.26 & 1.112 & -3.40 &  1.14\\
APR4(d=44.3 km) & 1.71 & 6.07 & 1.096 & -4.81 &  1.59\\
APR4(d=50 km) & 1.82 & 5.76 & 1.047 & -3.54 &  1.31\\
\hline
SLy(d=40 km) & 1.53 & 6.83 & 1.140 & -3.80 &  1.33\\
SLy(d=44.3 km) & 2.02 & 7.46 & 1.111 & -5.06 &  1.74\\
SLy(d=50 km) & 2.34 & 7.51 & 1.056 & -4.16 &  1.55\\
\hline
H4(d=40 km) & 2.34 & 7.51 & 1.063 & -1.26 &  0.43\\
H4(d=44.3 km) & 1.56 & 5.82 & 1.075 & -2.67 &  0.93\\
H4(d=50 km) & 2.49 & 7.85 & 1.061 & -3.92 &  1.52\\
\hline
MS1(d=40 km) & 0.27 & 1.40 & 1.038 & -0.30 &  0.17\\
MS1(d=44.3 km) & 0.24 & 0.85 & 1.017 & -0.51 &  0.17\\
MS1(d=50 km) & 0.55 & 1.64 & 1.021 & -1.16 & 0.42\\
\hline
\end{tabular}\\
\caption{The first two columns represent the parameters $\Delta t$ and
  $\Delta\phi$ which minimizes Eq.~\ref{eq:al1min} for each model with
  $d<60$ km. The next two columns show the parameters $\eta$ and
  $\Delta\phi_2$ which minimize Eq.~\ref{eq:al2min}. The last column
  shows the effective merger time difference between the
  time-dilation aligned waveform and the original one $\Delta
  t_\mathrm{merger}=t_\mathrm{merger}(\eta-1)$}
\label{tab:al_par}
\end{centering}
\end{table}

The minimizing parameters for both alignment procedures, with the corresponding effective merger time difference, are reported in table \ref{tab:al_par}. In table \ref{tab:al_err}, instead, are reported the maximum phase differences, during the whole inspiral, between the aligned signals and the $d = \SI{60}{km}$ ones. In particular, the residual phase errors after the time dilation alignment (which corrects the effective merger time error) can be seen as lower bounds for the accumulated numerical errors by the evolution code, due to the finite grid resolution. As anticipated in sec. \ref{sec:extraction}, these are much higher than the errors coming from the waveform extraction procedure, after the application of the second order perturbative extrapolation formula \ref{eq:second_order}, even in the first part of the signal, contrary with what was found in \cite{Chu2015a} for binary black holes simulations.

\begin{table}
\begin{centering}
\begin{tabular}{|c|ccc|ccc|}
\hline
\multirow{2}{*}{EOS} &
\multicolumn{3}{c|}{Shift alignment (Eq.~\ref{eq:al1})} &
\multicolumn{3}{c|}{Dilatation alignment (Eq.~\ref{eq:al2})}\\

& \multicolumn{3}{c|}{$\mathcal{E}_{\phi_\mathrm{al_1}}$(rad)} &
\multicolumn{3}{c|}{$\mathcal{E}_{\phi_\mathrm{al_2}}$(rad)} \\

\cline{2-7}
& $d=50$ km & $44.3$ km & $40$ km & $d=50$ km & $44.3$ km & $40$ km\\

\hline
APR4 &   4.12 & 2.79 & 2.40 & 0.98 & 0.09 & 0.49 \\
\hline
SLy & 5.01 & 2.79 & 1.77 & 0.40 & 0.25 & 0.37 \\
\hline
H4 & 3.41 & 2.06 & 1.85 & 0.33 & 0.35 & 0.20 \\
\hline
MS1 & 0.91 & 0.19 & 0.41 & 0.09 & 0.23 & 0.10\\
\hline
\end{tabular}
\end{centering}
\caption{Maximum phase difference in the inspiral phase between
  waveforms from simulations starting from $d<60$ km and simulations
  with $d=60$ km, aligned according to Eq.~\ref{eq:al1} (first
  three columns) or Eq.~\ref{eq:al2} (second three columns).}
\label{tab:al_err}
\end{table}

\begin{figure}
	\begin{centering}
	\includegraphics[width=0.9\textwidth]{figs/EJ.pdf}
	\end{centering}
\vspace{-0.8mm}
\caption{Gauge invariant reduced energy over reduced angular momentum curves, for the simulations presented in figure \ref{fig:h_d}. The filled dots mark the binding energy and angular momentum at the time of the merger.}
\label{fig:EJ} 
\end{figure}

This picture, coming from the GW phase differences, is consistent with the one given by the radiated energy and angular momentum. A common technique is to analyse the gauge-invariant relation between the reduced radiated energy
\begin{equation}
E_r := \frac{\rnd{\frac{E}{M}-1}}{\nu} \label{eq:E_r}
\end{equation}
and the reduced radiated angular momentum along the z axis:
\begin{equation}
J_r := \frac{J_z}{M^2 \nu}, \label{eq:J_r}
\end{equation}
where $E = M_{AMD} - E_{gw}$ and $J_z = J_{ADM} - J_{gw}$, with $E_{gw}$ and $J_{gw}$ computed from the gravitational wave strain using eq. \ref{eq:dEdt} and \ref{eq:dJdt}. The initial ADM mass and angular momentum are taken from the output of the LORENE code, when importing initial data in the evolution code numerical grid. Finally, $M = M_1 + M_2$ is the sum of the two stars gravitational masses and $\nu = \frac{M_1M_2}{M^2}$ is the reduced mass divided by $M$. The $E_r(J_r)$ curves are shown in figure \ref{fig:EJ}, for each EOS and initial separation. They agree, in simulations with the same EOS, very well, until the start of the $d = \SI{40}{km}$ simulations. After that point the simulations with d = 60 km start to diverge, resulting in a merger (marked with the filled circles in figure \ref{fig:EJ}) at a higher energy and angular momentum. The other three simulations, instead, agree much better until the merger, and their merger energies and angular momenta are also close to each other. This has the natural interpretation that the simulations with a shorter effective merger time emit more energy and angular momentum during their faster approach to the merger, in which they carry out more orbits. In all the simulations starting from $d < 60$ km we observe a good agreement of the initial energy and angular momentum, after the short relaxation period, with the simulations starting from a larger interbinary distance, with a maximum difference of a few tenths of percent. This confirms again that, at least in the inspiral phase, our numerical evolutions, even at this resolution, are able to join the sequence of quasi-equilibrium states generated by the LORENE library. However, in the late inspiral and plunge phase, when the tidal effects start to be relevant, there are important differences.  

This common picture can be naively interpreted considering that higher finite-resolutions errors accumulate during the longer simulations. In particular, this would explain why the $d = \SI{60}{km}$ simulations are different from the other three, which are more similar, and why the phase differences are lower for the less compact stars (which have also a lower merger time). However, other explanations are possible. 

A contribution to the difference between simulations starting from different separations can come from tidal effects, which are already important at the start of the $d = \SI{40}{km}$ simulations, when the differences starts to show up in the $E_r(J_r)$ curves. As shown before, there are higher deviations in the $d = \SI{60}{km}$ simulations with respect to the ones starting from further inward in the more compact stars. Tidal deformations developed in the dynamical evolution of models starting from a lower frequency could be different from the ones present in close binaries initial data, which are computed, with an approximate gravitational potential, by a spectral code and then interpolated in our cartesian grid. On the other hand, small tidal deformations could be present also in the d = 60 km initial configurations, but they could be under-resolved when we interpolate those initial data in our evolution code grid at our resolution. In ref. \cite{Tsokaros2016} has been shown that even small differences in initial data could be amplified during the evolution of the highly non-linear equations needed to describe BNS mergers in full general relativity, and this is particularly important for the the evolution of initial models with the higher merger times, which show the larger deviations between different initial separations. 

The role of dynamical tidal effects was also recently underlined in ref. \cite{Hinderer2016,Steinhoff2016a}. These
effects develop from the interaction of the tidal field with the stars quasi-normal modes of oscillation. These dynamic tides could be developed during a many orbits evolution, but are not considered in the calculation of the initial data used here. However, they should be more important for the less compact stars.

Another possible source of error is linked with the close binary initial configuration. In ref. \cite{Suh:2016ctx} it is shown that initial BNS data computed in the conformal flatness approximation for the gravitational potential need to be evolved for more than 3 orbits to reach a true, stable, quasi-equilibrium configuration. This condition is not (or is barely) satisfied for the d = 40 km simulations (see table \ref{tab:models3}).

From the available data it is impossible to disentangle all these error sources, evaluate their relative contributions, and fully answer to the questions posed at the beginning of this section. However, showing that there could be problems (or hidden physical effects) in many orbits BNS simulations, that their predicted inspiral GW signals do not match with the ones from shorter simulations, that these differences are EOS-dependent (they look higher in the more compact stars), and that they manifest themselves as a shorter effective merger time in the simulations starting from closer binaries configurations, are important and interesting first results, which can stimulate further enquiries about these subjects. In particular, new sequences of simulations of the same model starting from different separations, with better spatial resolution, are needed. More accurate numerical methods could also be employed, in particular Z4-family formulations of the Einstein equations (see sections \ref{sec:Z4}), for their constraints damping property, and higher than second order methods for the hydrodynamics, as used for example in \cite{Radice2014,Tsokaros2016,Bernuzzi2016}. These more advanced numerical techniques, reducing the dynamical evolution errors, should reduce also the differences between the simulations with different initial interbinary distance, if that turns out to be their the dominant source. Another interesting direction would be to repeat this kind of simulations with a larger EOS sample, to better check the results dependency on the stars deformability, and, with that, on tidal effects. Also important is to perform a convergence study, to check if the differences reduces increasing the resolution. All these possible future studies, however, are very computationally expensive.

Finally, one last interesting direction is to compare different techniques for producing initial data, as already done in ref. \cite{Tsokaros2016}. This is made somewhat difficult by the fact that the LORENE code is the only BNS initial data code publicly available. However, being an object-oriented code, it is easy to add to it new algorithms for initial data generations, such as the use of the extended conformal thin sandwich formulation \cite{York1999,Pfeiffer2003}, which tries to overcome the conformal flatness limitation.

In section \ref{sec:PM_d} I will discuss more results coming from this comparison, about the impact of the initial interbinary distance on the post-merger GW signal.








