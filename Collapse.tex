\subsection{Collapse to black hole}
\label{sec:BH}
To conclude this chapter about the post-merger BNS dynamics, it is interesting to take a closer look to models collapsing to black hole, during the simulated time. In order to do so, I will analyse, following ref. \cite{DePietri2016}, two good representatives of the possible paths to collapse: the equal mass systems with the SLy EOS and a baryonic mass $M = \SI{1.6}{\msun}$ for each star  
(which collapses promptly after the merger) or with $M = \SI{1.5}{\msun}$ (which collapses after a short post-merger HMNS phase).

The black hole formation is captured looking at regular intervals for the formation of an apparent horizon, with the Einstein Toolkit module \codename{AHFinderDirect} \cite{Thornburg2004}. In models where a black hole collapse is expected (from the output of poorer resolution simulations), an additional grid refinement level, with resolution $dx$ half of the finer grid resolution used during the inspiral evolution, is added, when the minimum lapse on the grid gets below a critical value. 

\begin{figure}
\begin{centering}
\begin{subfigure}[h]{.5 \hsize}
  \includegraphics[width=.9\hsize]{figs/img_disk1.png}\\
\vspace{-6mm}
\caption{Accretion disc density for model SLy1.6vs1.6}
\end{subfigure}%
\begin{subfigure}[h]{.5 \hsize}
	\includegraphics[width=.9\hsize]{figs/img_disk2.png}\\	
\vspace{-6mm}
\caption{Accretion disc density for model SLy1.5vs1.5}
\end{subfigure}%
\end{centering}
\vspace{-2mm}
\caption{Density profiles for the accretion discs formed in a model promptly collapsing after the merger (SLy1.6vs1.6, left figure), or collapsing after a short HMNS phase (SLy1.5vs1.5, right figure). They are taken at the end of the simulations, at least \SI{30}{ms} after the merger.}
\label{fig:BH}
\end{figure}

Figure \ref{fig:BH} shows a snapshots of the black hole-disc system evolution, at the end of each simulation, 30 and 34 ms after the merger. The most striking difference is that in the model with prompt collapse, very little mass is left in the disc around the black hole, which is quickly absorbed, forming a small disc, limited in space and axisymmetric. In the model with delayed collapse, instead, much more matter have been expelled from the central NS core during the HMNS evolution, causing the formation of a massive disc, with a large spatial extension, and which still has a spiral structure in its inner, higher density, region, even ~\SI{30}{ms} after the collapse.

\begin{figure}
\begin{centering}
\begin{subfigure}[h]{.5\hsize}
	\includegraphics[width=\textwidth]{figs/MaxDensityBHmodels.pdf}
\vspace{-0.6mm}
\caption{Maximum density}
\label{fig:BHrho}
\end{subfigure}%
\begin{subfigure}[h]{.5\hsize}
	\includegraphics[width=\textwidth]{figs/MJ_budget_BHmodels.pdf}
\vspace{-0.6mm}
\caption{Energy and angular momentum budget}
\label{fig:BH_EJ} 
\end{subfigure}%
\end{centering}
\vspace{-0.6mm}
\caption{Properties of the black hole-disc system: the left figure shows the maximum density left on the grid, which is lower than the neutron drip level, independently on collapse time and grid resolution. The right figure, instead, shows the energy and $z$ direction angular momentum contributions, before and after the collapse (see text for their computation).}
\end{figure}

In figure \ref{fig:BHrho} is reported the evolution of the maximum density on the numerical grid in both models. The most interesting feature to notice is that, independently of the path to collapse, the density of the leftover matter in the accretion disc quickly gets lower than the neutron drip line. This means that, for modelling collapsing BNS systems, it is important to use an equation of state able to give a realistic description also to low density matter. The simplest way, adopted in my work, is to use the full 7 pieces piecewise polytropic parametrization of \cite{Read2009}, with the four lower-density segments taken from the SLy EOS \cite{Douchin2000}. This is different from what is done in most of the numerical relativity literature (see, for example, ref. \cite{Takami2014,Hotokezaka2013a,Dietrich2015}), where only the four highest density pieces are used, extrapolating the one which describes the inner crust down to the lowest frequencies, as already discussed in section \ref{sec:eos}. This approach is perfectly fine when studying a massive neutron star post-merger remnant, whose evolution is mainly driven by the high-density region dynamics, although some care should be taken when building empirical relations linking some GW signal characteristics to the initial star properties, since the stellar radius can be sensitive to the low density EOS. However, figure \ref{fig:BHrho} proves that it could lead to wrong results when analysing also the accretion disc dynamics after collapse.

It is interesting to analyse the energy and angular momentum balance in collapsing models, presented in figure \ref{fig:BH_EJ}, in order to both verify the code ability to conserve those quantities throughout the evolution, even when a black hole appears on the numerical grid, and how the different components contribute to the overall energy and angular momentum balance. The initial values are taken from the output of the LORENE code, when importing BNS initial data on the evolution code numerical grid. The energy and angular momentum radiated in gravitational waves are computed integrating eq. \ref{eq:dEdt} and \ref{eq:dJdt}, as already done several times, for example to construct the $E_r(J_r)$ curves of fig. \ref{fig:EJ}. The disc mass and z angular momentum, instead, are computed tracking two quantities, which are exact only in the case of axial and temporal symmetry, and which can easily be computed during the numerical evolution:
\begin{align}
M\ &=\ \int{\rnd{2T^{\mu}_{\nu}- \delta^{\mu}_{\nu}T^{\alpha}_{\alpha}}\xi^{\nu}_{(t)} d^3\Sigma_{\mu}} \\
J_z\ &=\ \int{T^{\mu}_{\nu}\xi^{\nu}_{(\phi)} d^3\sigma_{\mu}}, 
\end{align}
where $\Sigma_{\mu}$ are the three-dimensional slices of fixed coordinate time $t$ and $\xi^{\nu}$ are the two Killing vectors
\begin{align}
\xi^{\nu}_{(t)}\ &=\ (1,0,0,0)\\
\xi^{\nu}_{(\phi)}\ &=\ (0, \cos{\phi},\sin{\phi},0).
\end{align}
This formalism have been already used in the case of isolated stars \cite{Franci2013b}. The required stationarity and axisymmetry conditions are almost satisfied at the end of the simulations discussed here. Finally, the mass of the black hole is computed on the apparent horizon surface using the isolated horizon formalism \cite{Dreyer2003}, implemented by the Einstein Toolkit thorn \codename{QuasiLocalMeasures}.

For the promptly collapsing model, the accretion is already stopping at the end of the simulation time, while in the delayed collapse case the black hole mass is still increasing, and longer simulations are needed to reach an equilibrium configuration. Like in the previous section \ref{sec:PM_energy}, it is necessary to remark that the simulations presented here do not take into account important effects like magnetic fields, neutrino emission and a proper treatment of the EOS thermal component, therefore they are only a first approximation, useful as a benchmark for future numerical studies involving a better treatment of microphysical aspects.

Another important remark is linked with the collapse time of models with a delayed collapse. It was found to be extremely sensitive to some simulation parameters, like the spatial grid resolution (the SLy1.5vs1.5 model collapses after $,$ ms from the merger at resolutions $,$, showing a non-convergent behaviour of the collapse time), or the choice for the EOS thermal component. That last consideration was also pointed out in ref. \cite{Hotokezaka2013a}, where simulations with different choices for $\Gamma_{th}$ were confronted. The difference in $\Gamma_{th}$, from $1.8$ to $2.0$, is the most probable cause also for the fact that in ref. \cite{Takami2015} a BNS model similar to our SLy1.5vs1.5 does not collapse in their simulated \SI{25}{ms} of post-merger evolution. It should also be noted that the results about the disc characteristics and the accretion process are likely to be influenced also by the choice for the artificial atmosphere density imposed by the hydrodynamics code. We did a conservative choice of $\rho_{min} = \SI{6e6}{g/cm^3}$, but even lower values for the minimum accepted density should be tried in order to get more accurate estimates of the disc properties, in particular for promptly collapsing models.