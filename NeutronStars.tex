\section{Neutron stars}
With the term \quotes{Neutron star} (NS) we intend nowadays a compact star with a mass approximately between $1$ and $3 M_{\odot}$, a radius in the range $9-15$ km and a central density which is 3 to 10 times the nuclear equilibrium density $n_0 = \SI{0.16}{fm^{-3}}$ \cite{Lattimer2004}, in which the gravitational pressure cannot be compensated by the electrons fermi gas pressure, like in a white dwarf, but is, instead, equilibrated by the strong nuclear interactions. A neutron star interior is neutron-rich, although, despite the name, a fraction of protons is still present (and a corresponding fraction of electrons and/or muons to neutralize the matter), and more complex nuclei can be found in the external layer, called \quotes{crust}, as well other states like mesons, hyperons \cite{Lackey2006}, and even deconfined quarks \cite{Weissenborn2011} could appear in the inner core, at densities above $n_0$ (see next subsection for more details).\\
Neutron stars are among the most dense objects in the universe. The matter is held together by a strong gravitational field, for witch a correct treatment of general relativistic effects is important: for a typical neutron star with mass $\SI{1.4}{\msun}$ and radius \SI{10}{\km}, the radius is only 2.4 times the \sch\ radius of a non-rotating black hole with the same mass. This gravitational field cannot be compensated only by the Fermi pressure of a free Neutron gas, as already demonstrated by Oppenheimer and Volkov and independently by Tolman in 1939 \cite{Oppenheimer1939,Tolman1939}, because it will lead to a neutron star maximum mass of $\SI{0.7}{\msun}$. The pressure to sustain the star against gravitational collapse is given, instead, by repulsive nuclear forces \cite{Harrison1965}.

Neutron stars are born from the collapse of massive stars at the end of their life cycle, when the gravitational force cannot be sustained anymore by the internal pressure due to the thermonuclear reactions fuelling the star \cite{Burrows1986}. When the inner density of the star reaches the nuclear equilibrium density $n_0$, the stellar matter bounces back, producing a shock wave generated at the outer layer of the inner stellar core. The inner core, in this first phase, is hot, optically thick for neutrinos and lepton-rich. It is still not clear which is the mechanism responsible for the reviving of the shock front, which first halts at around $100-200$km from the star center, in order to have a successful supernova explosion. The main candidates are neutrinos emitted in the core and then reabsorbed in the stellar medium \cite{Bethe1985,Ott2013,Hanke2013} or magnetic instabilities redistributing angular momentum and developing turbulence \cite{Mosta2014a,Mosta2015}. If the shock front gets revived, the stellar envelop is stripped from its center, leaving behind a proto-neutron star. In the first $\simeq 10$ ms it undergoes a highly dynamical phase dominated by turbulence and hydrodynamical instabilities, during which energy and angular momentum are emitted, mainly by neutrino radiation. During this first phase, stellar  oscillation modes can be excited, and they will be responsible for the emission of gravitational waves \cite{Ott2009,Ferrari2003,Burgio2011,Fuller2015}. The neutrino emission is linked with electron captures, which deleptonize the star, leaving it neutron-rich. In the following phase, called the ``Kelvin-Helmholtz" phase, the proto neutron star evolves in a quasi-stationary manner, cooling down, shrinking, slowing down its rotation rate, and becoming transparent to neutrinos \cite{Pons1999,Camelio2016}. During the collapse, the magnetic field of the progenitor star increases by several orders of magnitude, mainly due to flux conservation, but also due to the winding linked with the star differential rotation. In regular neutron stars the magnetic filed reaches values around $10^8 - 10^{12}$ Gauss. A special class of neutron stars, called ``magnetars", have magnetic fields up to $10^{15}$ Gauss. The magnetar formation process is still unknown, but is believed to be linked with magnetic instabilities which develop in the protoneutron star after the stellar bounce \cite{Mosta2015}.

Due to their external dipolar magnetic field, which could be misaligned with the rotation axis, several neutron stars can be observed as pulsars, emitting regular, pulsated electromagnetic signals in the radio band \cite{Lorimer2008,Manchester2005} (but, in some cases, also in X-rays and even gamma-rays \cite{Abdo2013,Caraveo2013}). This pulses are due to the electromagnetic radiation emitted by charged particles accelerated along magnetic field lines. Each pulse is visible when the star magnetic axis (and then its radiation beam) crosses the observer's line of sight, therefore the pulsation period is equal to the neutron star rotation period. The first experimental discovery of a neutron star happened in 1968 in the Mullard Radio Astronomy Observatory \cite{Hewish1968}.


In order to compute the equilibrium configuration for a non-rotating neutron star, one has to solve the Tolman-Oppenheimer-Volkov (TOV) equations, which, in the simplified modelling of the star as a barotropic fluid (valid for a cold neutron star, which has already cooled down after the progenitor collapse), are:
\begin{align}
&\d{P}{r} = \rnd{\rho\rnd{1+\epsilon}+p}\frac{m + 4\pi r^3 p}{r\rnd{r-2m}},\\
&\d{m(r)}{r} = 4\pi \rho\rnd{1+\epsilon} r^2.
\end{align}
Where all variables are functions of the single independent variable $r$ (because of spherical symmetry). This system must be closed by a prescription for the (barotropic) equation of state of the matter, in the form $P = P(\rho)$.

An important information which can be gathered from solving the TOV equation is the mass-radius relationship for a cold neutron star given a particular EOS model. This can help confronting different proposals for the neutron star EOS with experimental data. In order to do so, it is important to be able to measure the masses and the radii of observed neutron stars \cite{Ozel2016}. The masses can be measured from pulsars in binary systems, for which the orbital parameters are determined by pulsar timing and accounting for the Doppler effect \cite{Ozel2012}. From those Keplerian parameters, it is possible to construct a mass function:
\begin{equation}
f\ =\ \frac{\rnd{M_c \sin{i}}^3}{M^2_T}\ =\ \rnd{\frac{2\pi}{P_b}}^2 \rnd{a \sin{i}}^3,
\end{equation}
where $M_T = M_p + M_c$ is the total mass of the system, $M_p$ is the pulsar mass and $M_c$ is the companion mass, $P_b$ is the orbital period, $a$ is the semimajor axis and $i$ is the inclination angle between the orbital angular momentum of the system and the line of sight.

The mass function has 3 unknowns (the two star masses and the angle $i$), therefore two more equations are needed for deriving the mass values.
These come from the so called \quotes{post-Keplerian} (PK) parameters, which measure relativistic corrections to the Keplerian orbit of the binary. The five PK parameter used in practice are:
\begin{enumerate}
\item The periastron rate of advance $\dot{\omega}$, analogous to the perihelion advance of Mercury's orbit, it can be measured precisely in highly eccentric systems after a long observation period. From its measurement the total mass of the system can be constrained:
\begin{equation}
\dot{\omega}\ =\ 3\rnd{\frac{P_b}{2\pi}}^{-5/3} M_T^{2/3}\rnd{1-e^2}^{-1}
\end{equation}
\item The Einstein delay $\gamma$, due to the gravitational redshift and the time dilation effect present in eccentric orbits. It also requires high eccentricities and long time observations for a precise measurement.
\begin{equation}
\gamma\ =\ e\rnd{\frac{P_b}{2\pi}}^{1/3}M_T^{-4/3} M_c \rnd{M_p+2M_c}
\end{equation}
\item The orbital period derivative $\dot{P}_b$, which is negative due to the energy and angular momentum emission in gravitational waves. It is measurable in double neutron star systems only, after years of observations.
\item The range $r$ and the shape $s$ of Shapiro delay \cite{Shapiro1964}, which is the delay of the pulsar signal due to its passage into the spacetime curved by the gravitational field of the companion star. The Shapiro delay measures of NS masses are the most accurate one to date, including for example PSR J1614---2230 \cite{Demorest2010}, which was the first observed neutron star with a mass close to \SI{2}{\msun}. The Shapiro delay parameters are easier to measure for systems with a massive companion and with a high inclination angle.
\begin{align}
r &= M_c
&s = a\sin{i} \rnd{\frac{P_b}{2\pi}}^{-2/3}M_T^{2/3}M_c^{-1}.
\end{align}
\end{enumerate}
I want to remark that the relationship between the star masses and post-Keplerian parameters is dependent upon the choice of an underlying theory of gravity (which, in the case of the formulas written before, is General Relativity). This means that those mass measurements cannot be used as a test of GR, but instead assume its validity, even in the strong-filed regime of the neutron stars interior.

The neutron star radii, instead, are more difficult to measure directly. The current most common technique is based on spectroscopic measurements of the neutron stars angular size, based on their flux of surface thermal emission \cite{Heinke2013,Ozel2013,Ozel2016}. These observations are complicated by the need for a general relativistic treatment (neutrons stars lense their own surface emission \cite{Pechenick1983}), the presence of non-thermal magnetosphere emissions, which are very difficult to model, and the difficulty to measure the NS distance. A preferred laboratory for radius measurements are quiescent low-mass X-ray binaries (qLMXRB), in which, during the quiescent phase, the mass accretion from the companion to the pulsar ceases, reducing the non-thermal emission background \cite{Brown1998}. Another interesting technique is to measure photospheric radius expansion events due to X-ray thermonuclear bursts \cite{Lewin1993}. A third, frequently used, analysis to infer the NS radius is to model the periodic oscillations in the pulse profile originated by temperature anisotropies on the surface of rotating neutron stars \cite{Page1994,DeDeo2001}. These oscillations depend of the characteristics of the NS spacetime, hence on its mass and radius, which can be determined, given a theoretical model for the temperature profile on the stellar surface and the radiation beaming.

Different radial measurements have been obtained with these techniques, constraining the NS equation of state in different subregions of the M-R diagram (for example, radial measures from qLMXRB point to the presence of very compact stars with a radius around \SI{9.5}{\km} for a standard mass of $\SI{1.4}{\msun}$ \cite{Guillot2013,Lattimer2014}, while other analysis from pulse X-ray spectroscopy point to stars with larger radii, \SI{14}{\km} for stars with a mass around $1.5-1.8 M_{\odot}$). The problem is that all those techniques are highly depending on the surface emission and NS atmosphere modeling, which is still an active field of research. Forthcoming results in X-ray spectroscopy from missions like Athena \cite{Barret2013}, NICER \cite{Gendreau2012} and LOFT \cite{Feroci2012} should increase the precision in radius direct measurements.

Indirect measure of the NS radii can be obtained analysing the gravitational wave emission from binary neutron star coalescence, merger, and their post merger remnant (if there is not a prompt collapse to black hole). Most of this Ph.D. thesis is devoted to prepare the needed gravitational signal theoretical modelling for succeeding in that task. See sections \ref{sec:inspiral} and \ref{sec:PM_spectrum} for more details. 


\subsection{The neutron stars equation of state}
\label{sec:eos}
The equation of state of the nuclear matter inside neutron stars cores is still largely unknown. The extreme conditions present there (in terms of density, pressure, gravitational potential) can not be reproduced in experiments on Earth, and it is not possible to perform theoretical finite-density QCD calculations in that parameter region just from first principles \cite{Dey1998,Lugones2003}.

As a first approximation, we are interested in the equation of state of cold nuclear matter in beta equilibrium, which is suitable to represent neutron stars when they have cooled down from the protoneutron star phase (this is also the physical condition of neutron stars at the beginning of merger simulations, and during all the coalescent phase). Different techniques have been developed to compute EOS models. I will briefly illustrate, as an example, the ones used to develop the four EOSs that were employed in the simulations whose results will be analysed in chapter \ref{sec:results}.

The equation of state model should be able to describe the nuclear matter in a large density region, from the neutron star crust, where $\rho < \rho_0$ (with $\rho_0$ the nuclear equilibrium density) and the matter is composed only by the ordinary constituents, namely neutrons, protons, electrons, and simple atomic nuclei, up to the highest densities in the liquid inner core, where, for $\rho > 4\rho_0$, neutrons overlap and new non-nucleonic degrees of freedom can be present, such as hyperons, mesons condensates or deconfined quarks. Unfortunately, even for the crust case, calculation of an exact EOS staring from the bare two and three nucleon interactions experimentally measured in vacuum are not feasible, due to the complexity of the many-body problem concerning heavy nuclei immersed in a neutron gas, as happens in the NS crust. For this reason, one common technique is to use an effective nuclear hamiltonian in a mean-field scheme, containing effective two and three nucleon interactions. These effective nuclear interactions usually have a large number of free parameters,  fixed by fitting atomic nuclei properties measurements and the results of nucleon-nucleon scattering experiments, and then extrapolated to higher densities and nuclear matter asymmetries. In order to avoid problems linked with those extrapolations, the first EOS models we used, the \textbf{SLy} EOS \cite{Douchin2000,Douchin2001}, based on the Skyrme-Lyon nucleon-nucleon effective interaction, modifies the nuclear forces to take into account also the results of microscopic calculations for pure neutron matter, and also more recent experimental results on neutron-rich nuclei. In particular, it requires the consistency with the UV14+VII EOS of neutron matter in the range $n_0 < n < 1.5\ fm^{-3}$ and uses a general procedure for fitting the properties of doubly magic nuclei. The SLy EOS is widely accepted as the right modelling for the NS crust, which is described using the Compressible Liquid Drop Model, and for this reason all of our stellar models use the same parametrized version of SLy for describing the low density matter, but the matching point between the SLy crust and the core EOS changes for every high density EOS model. One big advantage of using the SLy EOS also for the high density matter is that it allows to be consistent employing the same effective nuclear interaction at all regimes, instead of having different approximated approaches in different regions of the star.

A similar EOS is the \textbf{APR4} model \cite{Akmal1998}, also based on an effective Hamiltonian approach, expanding it in one, two, three, ..., many body contributions, using a variational chain summation method. The nucleon-nucleon interaction is based on the Argonne $v_{18}$ potential, but relativistic boost corrections and three nuclear interactions are also included in the computed Hamiltonian.

A different approach, instead, is followed by the other two EOS models that were used in the simulations of \cite{Maione2016,Feo2016}, the \textbf{H4} EOS \cite{Lackey2006} and the \textbf{MS1} EOS \cite{Muller1996}. They are based on a relativistic mean field framework, in which the strong nuclear interactions are modelled as a meson exchange (scalar $\sigma$, vector $\omega$ and isovector $\rho$) between nucleons. The starting point is the construction of an effective lagrangian containing free-particle terms for each particle considered (nucleons, electrons, mesons), plus meson-baryon interaction terms and pertubative meson self-interactions. In this approach, too, free interaction parameters are fixed in order to reproduce the results of low-energy terrestrial experiments. However, some parameters remain unsufficently constrained, but can be further selected confronting the resulting neutron star models with observations, such as the neutron star maximum mass lower limit.

The H4 EOS \cite{Lackey2006} includes also hyperons, which in that model start to be produced at densities higher than $2 \rho_0$. The most relevant hyperons are $\Lambda$ and $\Sigma_-$, because they have the lowest masses and so are more easily produced. In this model new parameters are present, such as the ones controlling the meson-hyperon interactions. Some of them are fixed from the properties of lambda hypernuclei, while others, in particular the coupling constant between $\sigma$ mesons and hyperons, remain free to construct different EOSs models. In the H4 EOS these parameters are fixed in order to have the stiffest possible EOS sill consistent with the maximum neutron star mass and observed gravitational redshift of photons leaving a NS surface. 

It is common, in binary neutron star merger simulations, to parametrize the EOS as a piecewise polytrope, following the work of \cite{Read2009}. This was also done in the Parma gravity group simulations, analysed in chapter \ref{sec:results} and ref. \cite{DePietri2016,Maione2016,Feo2016}. We used seven polytropic pieces, each corresponding to a different density interval. The four lower density pieces are the same for each EOS and come from the fitting of the crust and low density matter modelled with the SLy EOS \cite{Douchin2000}. They represent, in increasing density order, a non-relativistic electron gas, a relativistic electron gas, the neutron drip regime, and the NS inner crust in the density interval between neutron drip and the nuclear saturation density. The three high density pieces, instead, are different for each NS core EOS model. In each density interval $\sq{\rho_{i},\rho_{i+1}}$, the pressure $P$ and specific energy density $\epsilon$ of a cold neutron star in beta equilibrium are given by:
\begin{align}
P_{cold} &= K_i\rho^{\Gamma_i}\\
\epsilon_{cold} &= \epsilon_i + \frac{K_i}{\Gamma_i-1}\rho^{\Gamma_i-1}
\end{align}
The coefficients $K_i$ and $\Gamma_{i}$ and the separation densities $\rho_i$ are reported in table \ref{tab:low_eos} for the four low density pieces and in table \ref{tab:high_eos} for the three high density pieces. The coefficients $K_i$ can be fixed given only $K_0$ and imposing the continuity of the pressure. Similarly, the coefficients $\epsilon_i$ are fixed to impose the continuity of the specific energy density, with $\epsilon_0 = 0$. The density threshold between the crust EOS and the inner core EOS ($\rho_4$) is different for each EOS model, and is selected, again, to impose continuity between the common low density EOS and the specific high density one. Its values are reported in table \ref{tab:high_eos} too.

\begin{table}
\begin{tabular}{|c|c|c|c|}
\hline
i & $\rho_i [g/cm^3] $ & $K_i$ & $\Gamma_i$ \\
\hline
\num{0} & - & \num{6.801e-11} & \num{1.584}\\
\num{1} & \num{2.440e7} & \num{1.062e-6} & \num{1.287}\\
\num{2} & \num{3.784e11} & \num{5.327e1} & \num{0.622}\\
\num{3} & 	\num{2.628e12} & \num{3.999e-8} & \num{1.357}\\
\hline
\end{tabular}
\caption{Parameters of the low density piecewise polytropic EOS. $\rho_i$ and $K_i$ are expressed in cgs units. Note that the units of $K_i$ depend on the corresponding values of $\Gamma_i$, so they are not directly comparable in magnitude.}
\label{tab:low_eos}
\end{table}

\begin{table}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
EOS & $\Gamma_4$ & $\Gamma_5$ & $\Gamma_6$ & $\rho_4 [g/cm^{3}]$\\
\hline
APR4 & 2.830 & 3.445 & 3.348  & $1.512 \times 10^{14}$\\
SLy  & 3.005 & 2.988 & 2.851  & $1.462 \times 10^{14}$\\
H4   & 2.909 & 2.246 & 2.144  & $0.888 \times 10^{14}$\\
MS1  & 3.224 & 3.033 & 1.325  & $0.942 \times 10^{14}$\\
\hline
\end{tabular}
\caption{Parameters of the high density EOS, parametrized as picewise polytrope, for the four different models analysed in this thesis, in increasing order of stiffness. $\Gamma_4$ and $K_4$ are the coefficients for the polytropic piece between the reported $\rho_4$ and $\rho_5 = 10^{14.7} g/cm^{3}$. $\Gamma_5$ and $K_5$ are, instead, the coefficients of the polytrope valid between $\rho_5$ and $\rho_6 = 10^{15} g/cm^{3}$.}
\label{tab:high_eos}
\end{table}

In \cref{fig:mr} are displayed the mass-radius relationships for a non-rotating neutron star with the four EOS mentioned before. It can be seen that all four EOS are consistent with the maximum mass limit of \SI{2.01}{\msun} imposed by the observation of PSR J0348+0432 \cite{Demorest2010}. The relativistic mean field EOSs are stiffer and lead to larger NS radii respect to the effective n-body nuclear interaction methods. Those four chosen EOSs cover all the range of most plausible NS radii, given the few observations available. In \cref{fig:density} are shown the density profiles of a \SI{1.4}{\msun} neutron star in a binary system with a distance of \SI{60}{\km} from the companion neutron star, taken from the initial data of the simulations presented in \cite{Maione2016}.


\begin{figure}
\begin{centering}
\begin{subfigure}[h]{.5 \hsize}
  \includegraphics[width=.9\hsize]{figs/TOV_plot.pdf}\\
\vspace{-6mm}
\caption{$M(R)$ curves}
\label{fig:mr}
\end{subfigure}%
\begin{subfigure}[h]{.5 \hsize}
	\includegraphics[width=.9\hsize]{figs/EOS_plot2.pdf}\\	
\vspace{-6mm}
\caption{$p(\rho)$ curves}
\label{fig:prho}
\end{subfigure}%
\end{centering}
\vspace{-2mm}
\caption{Left figure: mass over radius relationships for a cold non-rotating neutron star with the four EOS models analysed in this thesis. The grey horizontal line represents the maximum mass limit of the observed pulsar PSR J0348+0432 \cite{Demorest2010}.\\
Right figure: the top panel shows the pressure versus mass density curves for the same four EOSs. The bottom panel shows, instead, the adiabatic indexes $\Gamma$ for the piecewise polytropic representations of those EOSs.}
\end{figure}

\begin{figure}
\begin{centering}
  \includegraphics[width=.5\hsize]{figs/InitialData.pdf}\\
\end{centering}
\vspace{-2mm}
\caption{Density profiles for a cold neutron star in a binary system with a baryonic mass of \SI{1.4}{\msun} and a distance from the companion of \SI{60}{\km}. They are taken from the initial data of simulations presented in \cite{Maione2016}, using the APR4, SLy, H4 and MS1 EOSs. See also appendix \ref{sec:models} for more details on these and others simulated models initial configuration details.}
\label{fig:density}
\end{figure}

In figure \ref{fig:prho}, instead, is reported the pressure for each EOS, respect to the mass density of a star region. The relativistic mean field theory EOS (H4 and MS1) are joint at a lower density to the crust EOS (see also the values of $\rho_4$ in \cref{tab:high_eos}) and are stiffer (with a higher pressure support) in the interval between the nuclear density and $\rho_6 = 10^{15} g/cm^{3}$. This allows them to support stars with larger radii. The other two EOSs (APR4 and SLy), instead, are very similar (leading also to similar curves in the M(R) plot \cref{fig:mr}). They are softer in the $10^{14}-10^{15} g/cm^3$ density range, but have, instead, a larger pressure support at the highest densities. These density values beyond $10^{15} g/cm^3$, however, are not present in ordinary cold, irrotational, neutron stars in binary systems, which are used as initial data in the simulation analysed in this work. They could be reached, however, in the post-merger phase, if there is not a prompt collapse to black hole, especially in high total mass and equal mass models (see sec. \ref{sec:post_merger} and, in particular, figure \ref{fig:rho_max}). 

\subsection{Double neutron stars systems}
\label{sec:DNS}
Up to now, eleven double neutron star systems have been detected in our galaxy, through pulsar timing (the nature of the pulsar companion of one of those systems, PSR J1906+0746 as being a neutron star is still debated). Among those systems, only six have precise measurements of the masses of both stars, while for four others we have only an estimate for the total mass of the system. Two additional double neutron star systems have been detected in globular clusters, but the nature of the pulsar companion in one of them is not certain.

Table \ref{tab:dns} list all those detected DNS systems with their relevant parameters. In particular, for each double nutron star system, it was computed the eccentricity that the system will have when it will enter in the Advanced LIGO/Virgo band (whith a emitted GW frequency of 10Hz) with the following expression (approximated at the Newtonian level), from ref. \cite{Moore2016}:
\begin{equation}
\frac{10Hz}{f_{i}} = \rnd{\frac{e_{10Hz}}{e_i}}^{18/19}\rnd{\frac{1-e_{10Hz}^2}{1-e_i^2}}^{3/2}\rnd{\frac{304+121e_i^2}{304+121e_{10Hz}^2}}^{1305/2299},
\end{equation}
where $f_i$ is the frequency of the gravitational waves emitted by the binary in its current state, computed as twice the orbital frequency, $e_i$ is the current eccentricity of the orbit and $e_{10Hz}$ is the desired eccentricity at a GW frequency of $10Hz$. It was also computed the time $\tau_g$ needed for each binary to merge, only for systems for which the individual masses of each star are known, with the following approximate formula from ref. \cite{Lorimer2008}:
\begin{equation}
\tau_g \simeq \SI{9.83e6}{yr} \rnd{\frac{P_b}{hr}}^{8/3}\rnd{\frac{m_1+m_2}{M_{\odot}}}^{-2/3} \rnd{\frac{\mu}{M_{\odot}}}^{-1}\rnd{1-e^2}^{7/2},
\end{equation}
where $P_b$ is the binary rotation period, $m_1$ and $m_2$ are the masses of the two stars and $\mu = \frac{m_1 m_2}{m_1+m_2}$ is the reduced mass. 
\begin{table}
\begin{tabular}{|c|cccc|cc|cc|c|}
\hline
Pulsar & $M_T$ & $m_P$ & $m_C$ & q & $P_b$ & $e_i$ & $e_{10Hz}$ & $\tau_g$ & Ref.\\
& [$M_{\odot}$] & [$M_{\odot}$] & [$M_{\odot}$] & & [days] & & & [Gyr] & \\
\hline
J1765-2240 & - & - & - & - &13.638 & 0.303 & \num{2.574e-8} & - & \cite{Keith2009}\\
J1756-2251 & 2.570 & 1.342 & 1.231 & 0.92 & 0.320 & 0.181 & \num{7.207e-7} & 1.67 & \cite{Faulkner2005}\\
J1811-1736 & 2.57 & - & - & - & 18.779 & 0.828 & \num{3.044e-7} & - & \cite{Corongiu2007}\\
J1807-2500B*\textbf{g} & 2.572 & 1.366 & 1.206 & 0.88 & 9.957 & 0.747 & \num{3.053e-7} & 1032 & \cite{Lynch2012}\\
J0737-3039 & 2.587 & 1.338 & 1.249 & 0.93 & 0.102 & 0.088 & \num{1.118e-6} &  0.086 & \cite{Kramer2006}\\
J1829-2456 & 2.59 & - & - & - & 1.760 & 0.139 & \num{8.927e-8} & - & \cite{Champion2004}\\
J1930-1852 & 2.59 & - & - & - & 45.060 & 0.399 & \num{1.101e-8} & - & \cite{Swiggum2015}\\
J1906+0746* & 2.613 & 1.291 & 1.322 & 0.98 & 0.166 & 0.085 & \num{6.454e-7} & 0.31 & \cite{Lorimer2006}\\
B1534+12 & 2.678 & 1.333 & 1.345 & 0.99 & 0.421 & 0.274 & \num{8.853e-7} & 2.76 & \cite{Wolszczan1991}\\
B2127+11C\textbf{g} & 2.713 & 1.358 & 1.354 & 1.00 & 0.335 & 0.681 & \num{7.220e-6} & 0.22 & \cite{Anderson1990}\\
J1518+4904 & 2.718 & - & - & - & 8.634 & 0.249 & \num{3.234e-8} & - & \cite{Janssen2008}\\
J043+1559 & 2.734 & 1.559 & 1.174 & 0.75 & 4.072 & 0.112 & \num{2.93e-8} & 1456 & \cite{Martinez2015}\\
B1913+16 & 2.828 & 1.440 & 1.389 & 0.96 & 0.323 & 0.617 & \num{5.32e-6} & 99.6 & \cite{Hulse1975}\\
\hline
\end{tabular}
\vspace{-2mm}
\caption{List of all detected binary neutron star systems, in increasing order of total mass. For entries marked with * the neutron star nature of the pulsar companion is still debated. Entries marked with \textbf{g} have been observed in globular clusters. For each system we list the pulsar name, the total adm mass $M_T$, the gravitational mass of the pulsar $m_P$ and its companion $m_C$, the mass ratio $q$ (by convention always less than 1), the binary orbital period $P_b$ (from which the gravitational wave frequency can be computed as $\frac{2}{P_b}$), the system eccentricity in its current state $e_i$ and when the GW frequency will reach $10Hz$ ($e_{10Hz}$), the approximated merger time $\tau_g$ and the original article reference for the system discovery.}
\label{tab:dns}
\end{table}

From table \ref{tab:dns} it can be noted that the mass of neutron stars in double neutron star systems are constrained in a small range around \SI{1.35}{\msun}, and the mass ratios are close to one. This is different from the general distribution of neutron star masses,measured in binaries with regular stars or white dwarves, which is much broader and peaked at slightly higher masses \cite{Ozel2012,Ozel2016}. An exception to this trend, however, is the most recently observed BNS system with the pulsar J043+1559 \cite{Martinez2015}, which has a high total mass and a large mass asymmetry (with a mass ratio of 0.75, never observed before). The pulsar in that system has by far the highest mass measured for a neutron star in a BNS system (\SI{1.559}{\msun}), while its companion has the lowest one (\SI{1.174}{\msun}). The discovery of this system has been very important, because it testifies the need for studying with numerical simulations also binaries with large mass asymmetry (which have been neglected in the literature before 2015, with few exceptions \cite{Shibata2003c,Rezzolla2010,Hotokezaka2013a}) and lower (or higher) masses than what are usually considered (see for example ref. \cite{DePietri2016,Foucart2015a} for a first step in these directions).

Double neutron star system are formed form regular star binary systems. The first star collapses, creating a neutron star. This first neutron star accretes from the companion, spinning up becoming recycled. When the companion grows to become a red giant, a common envelope will engulf the neutron star. This will cause it to spiral-in, creating a tightly close binary. The energy released by accretion and friction in this process will lead the hydrogen envelope to be expelled, leaving a close binary formed by a neutron star and an helium star \cite{Dewi2003,Heuvel2007}. Due to large tidal effects this system has a perfectly circular orbit.
When the second star will undergo a supernova explosion, creating a second neutron star, a relevant fraction of its mass will be ejected. This would cause the binary to become unbound, unless the explosion impressed also a kick velocity to the newly born second neutron star. Studying the distribution of plausible kick velocities and mass ejecta for all the 13 BNS systems detected and reported in table \ref{tab:dns}, in ref. \cite{Beniamini2016} was shown that there is evidence for two different formation channels for BNS systems: a mechanism with high kicks and ejected mass, coming from a regular supernova, which produced the systems with high pulsar spin periods and high orbital eccentricity (like the Hulse-Taylor BNS B1913+16), and a different mechanism with low kicks and ejecta, coming from an electron-capture supernova, which produced the systems with faster rotating pulsars and lower eccentricities.
  