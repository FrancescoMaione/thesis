\section{Matter evolution}
\label{sec:hydro}
For simulating neutron stars, the spacetime evolution described in the previous section must be coupled with the hydrodynamical evolution for the matter. Working always in a 3+1 decomposition framework, with a spacetime metric given by \cref{eq:metric}, an additional important variable to consider is the fluid four-velocity $u^{\mu}$ (which, from a different point of view, is the velocity of an observer comoving with the fluid), which, like the Eulerian observer velocity $n^{\mu}$, has module $u^{\mu}u_{\mu} = -1$ and is timelike. The GRHD equations will be written in the Eulerian observer reference frame, so it is also important to consider the spatial projection $v^i$ on each hypersurface $\Sigma_t$ of the fluid four-velocity measured by the Eulerian observer. This is given by
\begin{equation}
v^i = \frac{\gamma^i_{\mu} u^{\mu}}{-u^{\mu}n_{\mu}} = \frac{1}{\alpha}\rnd{\frac{u^i}{u^0}+\beta^i}.
\end{equation}
It is interesting to note that in the case $\alpha = 1$ and $\beta^i = 0$ one recovers the special relativistic expression $v^i = \frac{dx^i}{dt} = \frac{u^i}{u^0}$, with $u^i = \frac{dx^i}{d\tau}$. With simple algebra, using the unitary of the $u^{\mu}$ module, it is easy to show that 
\begin{equation}
\alpha u^0 = \frac{1}{\sqrt{1-v_iv^i}} := W,
\end{equation}
where we have defined $W$ as the Lorenz factor of the fluid.

The next step is to consider the ideal-fluid energy momentum tensor:
\begin{equation}
T^{\mu\nu} = \rho h u^{\mu} u^{\nu} + p g^{\mu\nu},
\end{equation}
where $\rho$ is the mass density (contrary to the notation often used in the astrophysics literature, where the letter $\rho$ indicates the energy density), $h = 1 + \epsilon + \frac{p}{\rho}$ is the relativistic specific enthalpy, $\epsilon$ is the specific energy density and $p$ is the pressure. The equations to solve for evolving the hydrodynamical variables are:
\begin{align}
\nabla_{\mu} T^{\mu\nu} &= 0 \label{eq:T_cons}\\
\nabla_{\mu} (\rho u^{\mu}) &= 0 \label{eq:J_cons},
\end{align}  
where (\ref{eq:T_cons}) is the energy-momentum conservation and (\ref{eq:J_cons}) is the baryon number conservation. This system of equations should be closed with a choice for the matter EOS (see sec. \ref{sec:eos} for a discussion about the neutron star EOS and its piecewise polytropic approximation often used in numerical relativity), in the form $p = p(\rho, \epsilon, ...)$.

The code \codename{GRHydro} \cite{Mosta2014}, which is the official, open source, GRHD module in \codename{The Einstein Toolkit}, uses the so called \textit{Valencia formulation} of the GRHD equations \cite{Marti1991,Font1994,Banyuls1997a,Marti2003,Anton2006,Font2008}. It is a conservative formulation, in which instead of the \quotes{natural} five \textit{primitive} variables $\rnd{\rho,\epsilon,v^i}$ a set of derived \textit{conserved} variables is evolved, in order to be able to write the hydrodynamics equation in the conservative form
\begin{equation}
\partial_t \mathbf{U} = \partial_i \mathbf{F}^{(i)}(\mathbf{U}) + \mathbf{S}(\mathbf{U}), \label{eq:conservation}
\end{equation}
where $\mathbf{U}$ is the state vector containing the five conserved variables, $\mathbf{F}^i$ are the flux vectors, one for each direction, and $\mathbf{S}$ is the source vector, which does not contain derivatives of the state vector variables. A conservative (and strongly hyperbolic) formulation for the evolution equations helps with their numerical implementation, since any finite differences algorithm will automatically conserve the conserved variables in the theory and, moreover, thanks to Lax-Wendroff 1960 theorem, the numerical solution, if the code is in a convergent regime, will converge to a weak solution of the system (a solution to its integral representation). The conservative formulation is an essential hypothesis for this theorem \cite{Lax1960}. The Valentia formulation conserved variables are:
\begin{equation}
\rnd{\begin{array}{c}
D = \sqrt{\gamma} W \rho \\
S_j = \sqrt{\gamma} \rho h W^2 v_j\\
\tau = \sqrt{\gamma} (\rho h W^2 - p) - D. 
\end{array}
}
\end{equation}
The corresponding fluxes are:
\begin{equation}
F^i =
\rnd{\begin{array}{c}
D(\alpha v^i -\beta^i)\\
S_j (\alpha v^i - \beta^i) + p \delta^i_j\\
\tau (\alpha v^i -\beta^i) + p v^i
\end{array}
}
\end{equation}
And, finally, their source terms:
\begin{equation}
S =
\rnd{
\begin{array}{c}
0\\
T^{\mu\nu} \rnd{\partial_{\mu} g_{\nu j} + \Gamma^{\sigma}_{\mu\nu} g_{\sigma j}}\\
\alpha\rnd{T^{\mu 0}\partial_{\mu} \log{\alpha} - T^{\mu\nu}\Gamma^0_{\nu\mu}}
\end{array}
}.
\end{equation}
Besides obtaining convergence to the right physical solution and conservation in the numerical evolution of the physically conserved variables, another requisite for an evolution scheme for hydrodynamics is the ability to treat consistently also solutions with shocks. Any non-linear, hyperbolic, PDE can develop shocks even starting from smooth initial data. Therefore we need a numerical algorithm able to capture efficiently those shocks, without crashing the computer code or loosing accuracy. The starting point is a finite-volume scheme, which helps both highlighting the conservation properties of the system and dealing with discontinuous solutions. Its main point is evolving, instead of the point-valued conserved variables $U$, their volume average in a numerical cell
\begin{equation}
\bar{U} = \frac{1}{\Delta V}\int_{x^1}^{x^1+\Delta x^1}{\int_{x^2}^{x^2+\Delta x^2}{\int_{x^3}^{x^3+\Delta x^3}{ U dx^1 dx^2 dx^3}}}. \label{eq:Dbar}
\end{equation} 
Integrating equation (\ref{eq:conservation}) in a spatial numerical cell, and using Gauss divergence theorem, one gets:
\begin{align}
\partial_t \bar{U}(\vec{x},t) = -\frac{1}{\Delta x^1}\rnd{\bar{F}_{23}(x^1+\Delta x^1) - \bar{F}_{23}(x^1)} - \frac{1}{\Delta x^2}\rnd{\bar{F}_{13}(x^2+\Delta x^2) - \bar{F}_{13}(x^2)} + \label{eq:int_ijk}\\
- \frac{1}{\Delta x^3}\rnd{\bar{F}_{12}(x^3+\Delta x^3) - \bar{F}_{12}(x^3)} + \bar{S}(\vec{x},t), \notag 
\end{align}
where $\bar{F}_{ij}$ is the surface average of the flux $F$ on the $i,j$ boundary face of the numerical cell volume:
\begin{equation}
\bar{F}_{ij}(x^k) = \int_{x^i}^{x^i+\Delta x^i}{\int_{x^j}^{x^j+\Delta x^j}{F(\tilde{x}^i,\tilde{x}^j,x^k) d\tilde{x}^i d\tilde{x}^j}}.
\end{equation}
In order to show, in a simplified way, the key idea of the shock-capturing algorithm adopted in \codename{GRHydro} (the so called \quotes{Godunov method}, developed by Godunov in an appendix of his Ph.D. thesis \cite{Godunov1959}), it is useful to integrate eq. \ref{eq:int_ijk} also in time, in the arbitrary interval $\sq{t_n,t_n+1}$:
\begin{align}
\bar{U}_{ijk}(t^{n+1}) = \bar{U}_{ijk}(t^n) + \frac{\Delta t}{\Delta x^i}\rnd{\hat{F}_{i+1/2 j k} -\hat{F}_{ijk}} + \frac{\Delta t}{\Delta x^j}\rnd{\hat{F}_{i j+1/2 k} -\hat{F}_{ijk}} + \label{eq:int_tijk} \\
 + \frac{\Delta t}{\Delta x^k}\rnd{\hat{F}_{i j k+1/2} -\hat{F}_{ijk}} + \int{S d^3 x dt}, \notag 
\end{align}
where $\hat{F}_{ijk}$ is the \textit{numerical flux function}:
\begin{equation}
\hat{F}_{ijk} = \frac{1}{\Delta t}\int_{t^n}^{t^{n+1}}{\bar{F}_{ijk} dt}.
\end{equation}
Equation \ref{eq:int_tijk} is a recipe to evolve forward in time the hydrodynamical conserved variables from the time-level $t^n$ to the time-level $t^{n+1}$, but it is not (yet) a numerical scheme, it's an analytical expression obtained without any approximation. In order to compute the evolved variables at $t^{n+1}$, however, one needs to know the time-averaged flux $\hat{F}$ on each surface of the considered numerical cell boundary, which depends on the solution for conserved variables at times $t > t^n$. In order to construct an appropriate numerical evolution scheme, one needs an approximation for $\hat{F}$, which is robust to the presence of shocks in the solution. Godunov's idea was to compute $\hat{F}$ using the solution of a one dimensional Riemann problem at each boundary in each Cartesian direction. The Riemann problem is given by a one dimensional conservation-form hyperbolic PDE (like \ref{eq:conservation}, but without any source term), with the following discontinuous initial conditions (around the separation face $x=x_0$):
\begin{equation}
\bar{U}(x,t_0=0) = 
\grf{
\begin{array}{lcr}
U_L & if & x < x_i\\
U_R & if & x > x_i
\end{array}
}.
\end{equation}

The Riemann problem is invariant under similarity transformations:
\begin{equation}
\rnd{x,t} = \rnd{ax,at}, a > 0 \label{eq:similarity},
\end{equation}
and, therefore, its solution is self-similar and depends on the variable $\frac{x-x_0}{t-t_0}$. The solution of a Riemann problem for the hydrodynamics equations consists of constant states separated by rarefaction waves (continuous self-similar solutions of the PDE), shock waves (where all hydrodynamical primitive variables are discontinuous) and contact discontinuities (where only the density is discontinuous and all other variables are continuous, like the neutron stars surface). This solution strategy imposes a Courant factor (see sec. \ref{sec:curvature}), because the time step should be sufficiently small not to allow waves from neighbouring cells interfaces to interact:
\begin{equation}
\Delta t < \frac{\Delta x}{\abs{v_{max}}},
\end{equation} 
where $v_{max}$ is the maximum wave speed in any point of the grid at a given time step.

The original first-order Godunov's scheme used eq. \ref{eq:int_tijk} for the time evolution, computing the numerical fluxes solving Riemann problems at cells faces with initial conditions given by a piecewise constant reconstruction: each conserved variable, inside each cell (and therefore also at the cell boundary, from the appropriate side) takes a constant value equal to its cell volume average.

To go beyond first-order convergence, one has to adopt a \textit{High Resolution Shock Capturing} scheme (HRSC). First of all, in our finite-volume approach, one can approximate cell volume averages $\bar{U}$ and face surface averages $\bar{F}$ with their point-values at the numerical grid cell and face centres ($U_{ijk}$ and $F_{jk}$ respectively). This is exact up to second order in space (in the sense that the error made with this approximation is proportional to $\rnd{\Delta x}^2$). This is the approach adopted in \codename{GRHydro}, and in all current finite-volume GRHD codes. To go beyond second order convergence, one has to include additional (complicated) steps to compute from the evolved volume-averaged conserved variables their values at each grid point at the desired approximation order (because they are used in the energy-momentum tensor for the finite-difference curvature evolution, see sec. \ref{sec:curvature}), and also compute, from the point-valued fluxes given by the Riemann problem solution, the face-averaged numerical fluxes to be used in eq. \ref{eq:int_tijk} \cite{Etienne2010a}. For this reason, the only GRHD codes formally able to go beyond second order convergence adopt a different finite differences Flux-Vector Splitting approach \cite{Radice2012,Radice2014,Bernuzzi2016}.

To increase the temporal resolution, in HRSC methods one adopts the Method of Lines (see sec. \ref{sec:curvature}) starting from eq. \ref{eq:int_ijk}. The numerical flux terms $\hat{F}$ are computed as solutions of a Riemann problem also in this case, because, thanks to their self-similarity property, they are guaranteed to be constant in time at the cell interface $x = x_0$. In the most recent numerical relativity works a fourth order RK method is often chosen also for the hydrodynamical variables. Even if second and third order RK methods adapted to keep the equation conservation properties exist, following most of the recent literature we found global lower errors adopting the same (and the highest-order possible, given the highest spatial finite difference order adopted) RK method for both the spacetime and the matter evolution (see \cite{Reisswig2013a,Etienne2015}).

To increase the spatial resolution beyond first order, instead, different reconstruction methods have been developed, in order to compute the initial values $U_L$ and $U_R$ for the Riemann problem, at the left and right interface of each cell boundary, from the cells volume averages. The common structure of these methods is the reconstruction of the internal profile of primitive variables inside each numerical cell with a polynomial interpolation. From these profiles, the limits at the cell boundaries $P_L$ and $P_R$ are computed, and from them the corresponding conserved variables. The polynomial reconstructions are supplemented with some kind of limiter, which limits the reconstruction profile slope, reducing the method to first order convergence near shocks. These limiters allows to overcome the limitations of Godunov's theorem, which states that any linear, higher than first order, reconstruction method may induce spurious oscillations near shocks.

In particular for this thesis were tested the Piecewise Parabolic method (PPM) \cite{Colella1984}, which is third order convergent in smooth flows, the Monotonicity Preserving method (MP5) \cite{Suresh1997}, fifth order, and the Weighted Essentially Not Oscillatory method (WENO) \cite{Liu1994,Jiang1996}, also fifth order convergent. The latter was proven to be the best for archiving global second order convergence even at very poor resolutions in \cite{DePietri2016} (see appendix \ref{sec:convergence}, but also refs.\cite{Bernuzzi2012,Muhlberger2014,Bernuzzi2016} for similar findings about the WENO method). 
As explained in \cite{Baiotti2010,Mosta2014}, the use of a at least $N^{th}$ order convergent ODE solver for the time evolution and an at least $N^{th}$ order space discretization algorithm guarantees a global $N^{th}$ order convergence (both are prerequisites, since the time and space discretization steps are linked by the Courant condition \ref{eq:courant}) in a MoL framework, if the right hand side in eq. \ref{eq:conservation} is first order convergent in time. The use of a Riemann problem solution to evaluate the fluxes at cell boundaries allows this final condition to hold.

To summarize the last section, the evolution of the GRHD equations in \codename{The Einstein Toolkit} procedes as follows:
\begin{enumerate}
\item The source terms are computed from the primitive variables at cells centres;
\item The primitive variables are reconstructed to the left and right states near the cell faces;
\item The conserved variables are computed from the reconstructed primitives with simple algebraic expressions;
\item A Riemann solver algorithm finds an approximate solution to the Riemann problem at each interface, using the conserved variables right and left reconstructed states as initial conditions. The Riemann problem is solved independently along each spatial directions, finding all the flux components $\bar{F}^i$ of eq. \ref{eq:int_ijk};
\item The source and flux terms are summed up to build the right hand sides for the MoL time evolution;
\item At the same time, the primitive variables values at cell centres are used to construct the energy-momentum tensor, with which the right hand sides of BSSN-OK evolution equations are computed;
\item Both hydrodynamical conserved variables and curvature variables are evolved in time with a forth-order Runge-Kutta method;
\item From the cells centres values of the conserved variables in the new time-step the new primitive variables are computed, with a root-finding procedure, which checks also if they lie in a physically acceptable range.
\end{enumerate}

\subsection{Reconstruction methods}
\label{sec:recontruction}
\subsubsection{PPM}
The first reconstruction method that was tried, is the widely adopted PPM, which is still the most common choice for numerical relativity codes (as, for example, \cite{Yamamoto2008a,Baiotti2010}), and which was already used by the Parma gravitational physics group in single-star simulations \cite{Baiotti2007,Loffler2015}. The original PPM algorithm is slightly modified in \codename{GRHydro}, adopting a formulation specialised to evenly-spaced grids, using the simplest dissipation algorithm and an approximated flattening algorithm which needs only three stencil points instead of the four of the original procedure.

The core of the PPM reconstruction is the interpolation of the reconstructed primitive variable $a$ with a quadratic polynomial:
\begin{equation}
a_{i+1/2} = \frac{1}{2}(a_{i+1}+a_i) + \frac{1}{6} (\delta a_i - \delta a_{i+1}),\label{eq:ppm_interp}
\end{equation}  
where $\delta a_i$ is given by the approximation:
\begin{equation}
\delta a_i = min(\frac{1}{2}\abs{a_{i+1}-a_{i-1}}, 2\abs{a_{i+1} - a_{i}}, 2\abs{a_i - a_{i-1}})\cdot sign(a_{i+1} - a_{i-1}) 
\end{equation}
when
\begin{equation*}
(a_{i+1} - a_i)(a_{i}-a{i-1}) > 0
\end{equation*}
and is zero otherwise.

After this first step, the right and left states are equal $a^R_i = a^L_{i+1} = a_{i+1/2}$, and the solution will be third order convergent in space for smooth flows. This, however in not strictly monotonicity preserving (it could create new extrema in the reconstructed profile, which were not extrema of the original variable profile). In particular, it can happen that the interpolated $a_{i}$ is not between $a_i^L$ and $a_i^R$. Such problem arises when $a_i$ is a local maximum or minimum, or when it is close to one of the reconstructed face averages, and then the interpolating parabola \quotes{overshoots}. In such cases the reconstructed variables are modified:
\begin{align}
a^L_i = a^R_i = a_i\ &\mbox{if}\ (a_i^R - a_i)(a_i - a_i^L) \leq 0\\
a^L_i = 3a_i - 2a^R_i\ &\mbox{if}\ (a_i^R - a_i^L)\rnd{a_i - \frac{1}{2}(a_i^R+a_i^L)} > \frac{1}{6}\rnd{a_i^R - a_i^L}^2\\
a^R_i = 3a_i - 2a^L_i\ &\mbox{if}\ (a_i^R - a_i^L)\rnd{a_i - \frac{1}{2}(a_i^R+a_i^L)} < -\frac{1}{6}\rnd{a_i^R - a_i^L}^2.
\end{align}   
 Before the monotonicity preserving procedure, another two steps may be applied: first, contact discontinuities could be steepened, to ensure sharp solution profiles. Next, a flattening procedure near shocks is applied, in order to reduce the convergence in that case towards first order (locally), and avoid oscillations in the solution. The reconstructed variables are modified as follows:
\begin{equation}
a_i^{L,R} = \nu_i a_i^{L,R} + (\-\nu_i) a_i,
\end{equation}
where $\nu_i$ is an additional advection velocity, which produces the extra dissipation needed for avoiding oscillations near shocks:
\begin{equation}
\nu_i = max\rnd{0,1-max\rnd{0,\omega_2\rnd{\frac{p_{i+1}-p_{i-1}}{p_{i+2}-p_{i-2}}-\omega_1 }}},
\end{equation}
and, instead, $\nu_i=1$ in smooth flows. The criteria for detecting a shock in this case are:
\begin{equation}
\omega_0\ min(p_{i-1},p_{i+1}) < \abs{p_{i+1}-p{i-1}}\ \mathbf{and}\ v_{i-1}^x - v_{i+1}^x > 0. 
\end{equation}
This procedure is different from the original one of \cite{Colella1984}, but requires only thee points stencils, instead of four points ones, which would increase considerably the computational cost, especially when using mesh refinement.
\subsubsection{MP5}
Another reconstruction method which recently received some attention is the MP5 method \cite{Suresh1997}. It was adopted in the first higher-than-second order GRHD code \cite{Radice2012,Radice2014} and was found to be the most accurate in a simple single TOV star test in the \codename{GRHydro} presentation paper \cite{Mosta2014}. MP5 is based on a fifth-order polynomial interpolation, followed by a limiter designed to keep high order convergence also near solution extrema, distinguishing them from shocks, which is not possible to do in third-order methods like PPM, and to preserve monotonicity when adopting a Runge-Kutta scheme for the time evolution. The core interpolation is given by:
\begin{equation}
a^L_{i+1/2} = \frac{1}{60}\rnd{2a_{i-2}-13a_{i-1}+47a_i+27a_{i+1}-3a{i+2}}.
\end{equation}
The condition regulating if a limiter is applied is:
\begin{align}
\rnd{a^L_{i+1/2} - a_i}\rnd{a^L_{i+1/2}-a^{MP} \leq 0} \label{eq:MP}\\
a^{MP} := a_i + minmod\rnd{a_{i+1}-a_i,\alpha(a_i-a_{i-1}},
\end{align}
where $\alpha = 4$ is used. The minmod limiter function is given by:
\begin{align}
minmod(x,y) &= \frac{1}{2}(sign(x)+sign(y))min(\abs{x},\abs{y})\\
minmod(w,x,y,z) &= \frac{1}{8}(sign(w)+sign(x))\abs{(sign(w)+sign(y))(sign(w)+sign(z))}\times \notag \\ & \times min(\abs{w},\abs{x},\abs{y},\abs{z}).
\end{align}
If eq. \ref{eq:MP} is satisfied, a limiter is applied. To set it, four combinations of the interpolated variable are built:
\begin{align}
a^{AV} &= \frac{1}{2}(a_i+a_{i+1})\\
a^{UL} &= a_i + \alpha (a_i - a_{i+1})\\
a^{MD} &= a^{AV} -\frac{1}{2}D^{M4}_{i+1/2}\\
a^{LC} &= a_i +\frac{1}{2}(a_i-a{i-1})+\frac{4}{3}D^{M4}_{i-1/2}.
\end{align}
$a^{AV}$ stands for AVerage, $a^{UL}$ for Upper Limit, $a^{MD}$ for MeDian and $a^{LC}$ for Large Curvature. $D^{M4}_{i\pm1/2}$ are built, instead, starting from finite-difference expressions for the second derivatives of the reconstructed field:
\begin{align}
D_i^- &= a_{i-2} - 2a_{i-1} + a_i\\
D_i^0 &= a_{i-1} - 2a_i + a_{i+1}\\
D_i^+ &= a_i - 2a_{i+1} + a_{i+2}\\
D^{M4}_{i+1/2} &= minmod\rnd{4D_i^0-d_i^+,4D_i^+-D_i^0,D_i^0,D_i^+}\\
D^{M4}_{i-1/2} &= minmod\rnd{4D_i^0-d_i^-,4D_i^--D_i^0,D_i^0,D_i^-}.
\end{align} 
From those field combinations one can write the limited reconstructed variable:
\begin{equation}
a^L_{i+1/2} = a^L_{i+1/2} + minmod\rnd{a_{min}-a^{L}_{i+1/2},a_{max}-a^L_{i+1/2}},
\end{equation}
where
\begin{align}
U_{min} &= max(min(a_i,a_{i+1},a^{MD}),min(a_i,a^{UL},a^{LC}))\\
U_{max} &= min(max(a_i,a_{i+1},a^{MD}),max(a_i,a^{UL},a^{LC})).
\end{align}
To obtain the reconstructed variables at the right interface, instead, it is sufficient to use the same algorithm, substituting $\grf{a_{i-2},a_{i-1},a_i,a_{i+1},a_{i+2}}$ with  $\grf{a_{i+2},a_{i+1},a_i,a_{i-1},a_{i-2}}$.
\subsubsection{WENO}
The WENO reconstruction is an improvement on the Essentially Non Oscillatory (ENO) method \cite{Harten1987}. In the original ENO method, two interpolation polynomials, with different stencils, are used. Then, based on the field smoothness, one of the two is selected, in order to avoid the one containing the discontinuous solution near shocks. This idea allows to keep high order convergence even near shocks and local extrema, which is not possible with single stencil methods with limiters like PPM and MP5. However, selecting just one of the stencils, the ENO method does not archive the maximum convergence order for the number of stencil points used, and is computationally intensive, requiring a lot of logical operations. The idea behind the WENO method (first developed in \cite{Liu1994} and improved to reach fifth order convergence in \cite{Jiang1996}) is to use a combination of all the possible ENO reconstruction stencils, each weighed accordingly, with weight that tend to zero in the case of a discontinuity present in the corresponding stencil.
\begin{align}
a^{L,1}_{i+1/2} &= \frac{3}{8}a_{i-2} - \frac{5}{4}a_{i-1} + \frac{15}{8}a_i\\
a^{L,2}_{i+1/2} &= -\frac{1}{8}a_{i-1} +\frac{3}{4}a_i - \frac{3}{8}a_{i+1}\\
a^{L,3}_{i+1/2} &= \frac{3}{8}a_i + \frac{3}{4}a_{i+1} - \frac{1}{8}a_{i+2}.
\end{align} 
Each of these approximations is third-order convergent, but an appropriate combination, spanning all the 5 points stencil from $a_{i-2}$ to $a_{i+2}$, is fifth order convergent, when the weights $w^i$ are all different from zero:
\begin{equation}
a^L_{i+1/2} = w^1 a^{L,1}_{i+1/2} + w^2 a^{L,2}_{i+1/2} + w^3 a^{L,3}_{i+1/2}.
\end{equation} 
WENO weights have sum one $\displaystyle \sum_i{w^i}=1$ and are chosen starting from the so called \textit{smoothness indicators} $\beta^i$:
\begin{align}
\beta^1 &= \frac{1}{3}\rnd{4a^2_{i-2}-19a_{i-2}a_{i-1}+25a_{i-1}^2+11a_{i-2}a_i-31a_{i-1}a_i+10a_i^2}\\
\beta^2 &= \frac{1}{3}\rnd{4a_{i-2}^2-13a_{i-1}a_i+13a_i^2+5a_{i-1}a_{i+1}-13a_ia_{i+1}+4a_{i+1}^2}\\
\beta^3 &= \frac{1}{3}\rnd{10a_i^2-31a_ia_{i+1}+25a_{i+1}^2+11a_ia_{i+2}-19a_{i+1}a_{i+2}+4a_{i+2}^2}.
\end{align}
From the smoothness indicators, the weights are obtained as follows:
\begin{align}
w^i &= \frac{\bar{w}^i}{\sum_i{\bar{w}^i}}\\
\bar{w}^i &= \frac{\gamma^i}{\rnd{\epsilon+\beta^i}^2}\\
\gamma^i &= \grf{\frac{1}{16},\frac{5}{8},\frac{5}{16}}.
\end{align}
The problem with this weights is that $\epsilon$ is scale-dependent, and therefore setting a fixed value for it could lead to problems in simulating physical systems with a large variation in relevant scales (such as problems where turbulence or hydrodynamical instabilities are present). A solution to this was found in \cite{Tchekhovskoy2007}, and is adopted also in \codename{GRHydro}: modified smoothness indicators are used, which depend on the scale of the reconstructed field, measured by its $L^2$ norm in the considered stencil $\abs{\abs{a^2}}$. The new smoothness indicators are:
\begin{equation}
\bar{\beta^i} = \beta^i + \epsilon\rnd{\abs{\abs{a^2}}+1}.
\end{equation}
In \codename{GRHydro} a default value of $\epsilon = \num{1e-26}$ is used. As mentioned before, a stencil weights will go to zero (and, therefore, the corresponding stencil smoothness indicator will tend to infinity) when a discontinuity in the reconstructed field is present in its points, adaptively reducing the convergence order when needed, but keeping at least third order convergence near shocks.
\subsection{The Riemann solver}
\label{sec:HLLE}
In the Parma relativity group simulations (analysed in chapter \ref{sec:results}), the approximate Riemann solver from Harten, Lax, van Leer and Einfeld (HLLE) \cite{Harten1983,Einfeldt1988} was used (following what is done in most GRHD codes), because it is not computationally expensive, it is robust (see for example a comparison with the more accurate Marquina solver in \cite{Takami2015}, where the robustness of HLLE was found to be important for avoiding constraint violations at the boundary of the EOS politropic pieces (see sec. \ref{sec:eos})), and because it is the only Riemann solver in \codename{GRHydro} already extended for GRMHD evolutions, making it easier to develop a follow-up project including also magnetic fields.
Differently for other common Riemann solvers (like the Roe and Marquina solvers), which use the so called \textit{local characteristics approach} \cite{Marti1991,Banyuls1997a,Anton2006}, based on the Jacobian matrices spectral decomposition of a linearisation of the GRHD equations (considering all five characteristics of GRHD), HLLE uses a two wave approximations, considering only the maximum and minimum wave speeds $V_+$ and $V_-$ at both sides of an interface, which generate a single state between them. The Riemann problem solution for a conserved variable $U$, in a point labelled by the variable $\xi = \frac{(x-x_{interface})}{\Delta t}$ is then:
\begin{equation}
U = 
\left\{
\begin{array}{lcr}
U_L &if& \xi < V_-\\
U^{HLLE} &if& V_- < \xi < V_+\\
U_R &if& \xi > V^+ 
\end{array}
\right.\label{eq:hlle}
\end{equation}
The intermediate state $U^{HLLE}$ can be computed starting from a one-dimensional form of eq. \ref{eq:int_tijk} in a control volume around the considered cell interface:
\begin{equation}
\int_{x_L}^{x_R}{U(x,t+\Delta t) dx} = \int_{x_L}^{x_R}{U(x,t) dx} + \int_t^{t+\Delta t}{F(U(x_L,t') dt'} - \int_t^{t+\Delta t}{F(U(x_R,t') dt'}. 
\end{equation}
The right hand side can be directly evaluated using the reconstructed variables $U_L$ and $U_R$ and the corresponding fluxes $F_L = F(U_L)$ and $F_R = F(U_R)$:
\begin{equation}
\int_{x_L}^{x_R}{U(x,t+\Delta t) dx} = x_R U_R - x_L U_L + \Delta t(F_L-F_R).\label{eq:uhlle_rhs}
\end{equation}
The left hand side, instead, can be divided into three parts, one at the left of the fastest left-going wave, one between the two waves, and one at the right of the fastest right-going wave:
\begin{align}
\int_{x_L}^{x_R}{U(x,t+\Delta t) dx} &= \int_{x_L}^{\Delta t V^-}{U(x,t+\Delta t) dx} + \int_{\Delta t V^-}^{\Delta t V^+}{U(x,t+\Delta t) dx} +\\
&+ \int_{\Delta t V^+}^{x_R}{U(x,t+\Delta t) dx} = \notag\\
&= \int_{\Delta t V^-}^{\Delta t V^+}{U(x,t+\Delta t) dx} + (\Delta t V^- - x_L) U_L + (x_R - \Delta t V^+) U_R. \notag
\end{align}
Inserting this result into eq. \ref{eq:uhlle_rhs} and dividing both terms by $\Delta t (V^+-V^-)$ one gets, finally, an expression for the average exact Riemann problem solution between the slowest and the fastest wave at time $t+\Delta t$, which is used as definition for the HLLE intermediate state:
\begin{equation}
U^{HLLE} = \frac{1}{\Delta t (V^+-V^-)}\int_{\Delta t V^-}^{\Delta t V^+}{U(x,t+\Delta t) dx} = \frac{V^+ U_R - V^- U_L + F_L - F_R}{V^+-V^-}.
\end{equation}
To get from those solution states the corresponding numerical fluxes, one needs to invoke the \textit{Rankine-Hugoniot} condition: given a shock propagating along $s(t)$ with speed $V = \frac{ds(t)}{dt}$, its flux functions are related with its states by
\begin{equation}
F(U(s_L,t)) - F(U(s_R,t)) = V\rnd{U(s_L,t)-U(s_R,t)}. \label{eq:RH}
\end{equation}
It is straight-forward to derive eq. \ref{eq:RH}, starting from a one-dimensional representation of eq. \ref{eq:int_ijk} between $x_L$ and $x_R$, splitting the left hand side in two parts, one on the left of the discontinuity $s(t)$ and the other on its right:
\begin{equation}
\frac{d}{dt}\int_{x_L}^{s(t)}{U(x,t)dx} + \frac{d}{dt}\int_{s(t)}^{x_R}{U(x,t)dx} = F(U(x_L,t)) - F(U(x_R,t)).
\end{equation}
Using the formula for moving a time derivative inside an integral, one gets:
\begin{equation}
F(U(x_L,t)) - F(U(x_R,t)) = V\rnd{U(s_L,t)-U(s_R,t)} + \int_{x_L}^{s(t)}{\partial_t U(x,t)dx} + \int_{s(t)}^{x_R}{\partial_t U(x,t)dx}.
\end{equation}
Taking the limits $x_R \to s(t)$ and $x_L \to s(t)$, one recovers the Rankine-Hugoniot condition (\ref{eq:RH}). Applying it to eq. \ref{eq:hlle}, between the left and the intermediate state, or between the intermediate and the right state, one obtains to equivalent expressions for the HLLE numerical flux function:
\begin{align}
F^{HLLE} &= F_L + V^-\rnd{U^{HLLE}-U_L}\\
F^{HLLE} &= F_R + V^+\rnd{U_R - U^{HLLE}}.
\end{align}
Substituting the expression found for $U^{HLLE}$ in eq. \ref{eq:uhlle_rhs}, one finally finds:
\begin{equation}
F^{HLLE} = \frac{V^+ F_L - V^- F_R + V^+V^-(U_R-U_L)}{V^+-V^-}.\label{eq:flux_hlle}
\end{equation}
Of course, to get a numerical flux, one needs also to come up with a way for computing the minimum and maximum wave speeds. In \codename{GRHydro} this is done computing the maximum and the minimum between all the analytically-computed eigenvalues of the GRHD system of equations using the left and right reconstructed states. Those eigenvalues, along the i-th direction, are \cite{Font1994}:
\begin{align}
\lambda_0  &= \alpha v^i - \beta^i (\mbox{triple})\\
\lambda_{\pm} &= \frac{\alpha}{1-v^2c_s^2}\sq{v^i(1-c_s^2) \pm c_s\sqrt{(1-v^2)(\gamma^{ii}(1-v^2c_s^2) - v^iv^i(1-c_s^2))}} - \beta^i,
\end{align}
where $c_s^2 = \frac{\partial p}{\partial \rho}$ is the sound speed, computed from the equation of state. An important difference between these GR eigenvalues and the Euler's equations Newtonian ones is that in $\lambda_{\pm}$ (which in the Newtonian case are simply $\lambda_{\pm}^{Newt} = v^i \pm c_s$) there are also coupling with velocities in the other directions, through $v^2$.
\subsection{Conservative to primitive conversion}
The recovery of primitive variables from the evolved conservative ones, to be able to start the next time-evolution step, constructing the energy-momentum tensor and reconstructing hydrodynamical variables at cell interfaces, can not be done analytically, like the conversion from primitives to conservatives, but in pure hydrodynamics (without magnetic fields, or radiation) it is relatively straight-forward, requiring only a one-dimensional Newton-Raphson procedure to recover the unknown pressure.

First, the undensitised conserved variables are computed:
\begin{align}
\hat{D} &= \frac{D}{\sqrt{\gamma}} = \rho W\\
\hat{S_i} &= \frac{S_i}{\sqrt{\gamma}} = \rho h W^2 v_i\\
\hat{\tau} &= \frac{\tau}{\sqrt{\gamma}} = \rho h W^2 - P - \hat{D}.
\end{align}
Next, two additional auxiliary variables are defined:
\begin{align}
Q := \rho h W^2 = \hat{\tau} + \hat{D} + P\\
\hat{S}^2 := \gamma_{ij} \hat{S}^i\hat{S}^j = \rnd{\rho h W}^2\rnd{W^2}-1,
\end{align}
where $\hat{S}^2$ is known from the evolved variables, and $Q$ depends only on the unknown pressure $P$. Given the relation between $Q$ and $\hat{S}$, one can construct expressions for the primitive variables $\rho$ and $\epsilon$ which also depend only on $P$:
\begin{align}
\rho &= \frac{\hat{D}\sqrt{Q^2-\hat{S}^2}}{Q}\\
\epsilon &= \frac{\sqrt{Q^2-\hat{S}^2} - PW - \hat{D}}{\hat{D}},
\end{align}
where the Lorentz factor $W$ can be expressed by:
\begin{equation}
W = \frac{Q}{\sqrt{Q^2-\hat{S}^2}}.
\end{equation}
Given an initial guess for the new pressure, one can compute the new density and specific energy density. Given those, a pressure value $P(\rho,\epsilon)$ can be computed using the EOS. With an iterative procedure such as the Newton-Raphson method, one will try to minimize the residual between the pressure value used to compute $\rho$ and $\epsilon$ and the one obtained with the subsequent EOS call. This method, in particular, requires to know the pressure derivatives $\frac{\partial P}{\partial \rho}$ and $\frac{\partial P}{\partial \epsilon}$, which are given, again, by the equation of state. The \textit{con2prim} routines are also responsible to check if the resulting primitive values are physical, for example if the pressure and the densities are positive, and if the velocities are less than one. 