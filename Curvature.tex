\label{sec:curvature}
The numerical solution of hyperbolic PDEs (like Einstein's equations and the general relativistic hydrodynamics (GRHD) equations) requires dedicated algorithms which have been developed in the last thirty years. The starting principle is representing continuous functions of space and time at several discrete time steps, and, at each time step, in a discrete spatial grid, with a finite number of points, in order to be able to apply to the spacetime fields discrete operations which a computer is able to perform. The spatial grid (in our case, a three dimensional Cartesian grid) represents a finite region in space which we are interested to evolve in time. In the case of BNS simulations, the grid is much bigger than the initial distance between the two stars, because there is the need to extract gravitational waves far from the source (see section \ref{sec:extraction}) and to avoid (or at least delay) the interaction between the physical evolved fields and spurious matter and radiation infalling or bouncing back at the grid external boundaries.

In order to be evolved in time, equations must be written as a system of first order in time PDEs, in the form:
\begin{equation}
\partial_t U_i(t,\vec{x}) \ =\ RHS\rnd{U_i(t,\vec{x}), \partial_{\vec{x}} U_i(t,\vec{x}), \partial^2_{\vec{x}} U_i(t,\vec{x}),t,\vec{x}}, \label{eq:pde} 
\end{equation}
where $U_i$ is a vector of our evolved variables. We start setting up initial data for each variable in each grid point. The grid points (in each refinement grid, see later) are distanced one another by the uniform spatial resolution $\Delta x$. Then initial data are evolved from the initial time $t_n$ to the subsequent time $t_{n+1} = t_n+\Delta t$, using the so called \quotes{method of lines}, handled by the \codename{MoL} Cactus thorn. MoL consists in computing the right hand side of a PDE like eq. \ref{eq:pde} approximating the spatial derivatives with appropriate methods (which are discussed in the following sections). In this way, for each grid point, the fields $U_i$ depend only on the time variable $t$, leaving a system of ordinary differential equations. They can then be solved with standard numerical techniques, for example the fourth-order Runge-Kutta method \cite{Runge1895,Kutta1901}, which can be used in \codename{The Einstein Toolkit} to evolve the solution at time $t^n$, denoted with $U_i^n$, to time $t^{n+1}$, computing $U_i^{n+1}$ in four different steps:
\begin{align}
&U_i^{(0)} = U_i^n\\
&U_i^{(1)} = U_i^{(0)} + \frac{1}{2}\Delta t\ RHS(U_i^{(0)})\\
&U_i^{(2)} = U_i^{(0)} + \frac{1}{2}\Delta t\ RHS(U_i^{(1)})\\
&U_i^{(3)} = U_i^{(0)} + \Delta t\ RHS(U_i^{(2)})\\
&U_i^{(4)} = \frac{1}{6} \rnd{-2U_i^{(0)} + 2U_i^{(1)} + 4 U_i^{(2)} + 2 U_i^{(3)} + \Delta t\ RHS(U_i^{(3)})}\\
&U_i^{n+1} = U_i^{(4)}
\end{align}
The spatial resolution and the temporal resolution are not independent, but are linked by the so called Courant factor 
\begin{equation}
\eta = \frac{\Delta t}{\Delta x}.\label{eq:courant}
\end{equation} This is both needed for compatibility with special relativity and for archiving stability in the adopted numerical methods. In all simulations analysed in the next chapter a Courant factor of 0.25 has been chosen.

One problem of solving GRHD equations is the need of both a very large grid (in the simulations of chapter \ref{sec:results} it is a cube with an half-edge of approximatively \SI{1063}{\km} (\SI{720}{\msun} in geometric units)) and at the same time achieving a very high spatial resolution (low $\Delta x$) near the evolved stars, to be able to resolve all hydrodynamical features and to have low numerical dissipation. This problem was solved by Berger-Oliger Adaptive Mesh Refinement (AMR) \cite{Berger1984}, which in the Einstein Toolkit is implemented by the thorn \codename{Carpet}. In particular, a simpler, common choice, in BNS merger simulations, is to use only Fixed mesh refinement (FMR). This consists in having multiple nested grids, one inside the other, each (smaller) grid with a resolution double respect to the parent level (meaning two times the density of points, hence half the points distance $\Delta x$). As an example, the grid structure used in the simulations performed by the Parma group can be found in table \ref{tab:grid}. The code FMR handles the time evolution via subcycling. An example of this algorithm, for just two refinement levels, with spatial resolutions $\Delta x$ and $2\Delta x$, is:
\begin{enumerate}
\item The coarse level,  (which has also a larger temporal resolution) evolves one step in time, from $t$ to $t+2 \Delta t = t + 2 \eta \Delta x$;
\item At the fine level boundary, the coarse level points are interpolated in space (with fifth order polynomial interpolation) and time (with second order polynomial interpolation), to be used as boundary conditions for the fine level evolution (this step is called \textit{prolongation});
\item The fine level evolves two steps in time (from $t$ to $t+\Delta t$ and, after, from $t+\Delta t$ to $t+2\Delta t$), to be aligned with the coarse one;
\item The fields values in coarse level points whose position in space coincides also with a fine level point are copied from the field values in the latter (this step is called \textit{restriction}). Is possible to do just a simple copy, without interpolation, because \codename{Carpet} uses a vertex centred grid, which means that where a finer grid points exists, there is also a corresponding point with the same Cartesian coordinates in each coarser grid. 
\end{enumerate}

This FMR scheme, coupled with a multi-step time evolution like RK4, brings problems at refinement boundaries: the fine grid evolution will need also boundary conditions, provided by prolongation, at each intermediate time step in the RK4 algorithm. This will, however, reduce convergence to first order at refinement boundaries \cite{Schnetter2004}. In practice, BNS simulations without any additional technique to solve this problem showed a great loss of accuracy globally in preliminary tests which were performed. The solution adopted in \codename{Carpet} is to use buffer zones at refinement boundaries, which means evolving 12 grid points more for each edge of any finer refinement level (3 ghost points for each of the four RK4 substeps). At each RK4 step, a set of three points becomes invalid, and is not used any more. These points, although evolved regularly like any other one (instead of filling their values with prolongation), are used only as boundary for the evolution of the regular (inner) points in the finer level. This of course leads to higher computational and memory costs, and in particular worsen the code scaling ability when thousands of computing cores are used. A promising alternative solution, which requires a modification of the Runge-Kutta algorithm, but avoids the use of buffer zones, have been proposed in ref. \cite{Mongwane2015}, and will be tested and implemented in \codename{Carpet} in the near future.


\section{Curvature evolution}
\label{sec:adm}
The numerical solution of Einstein's equations, allowing in particular long-term black hole dynamical simulations, has been a challenge for many years, until the success of the first many-orbits BBH mergers simulations of 2005 \cite{Pretorius2005,Campanelli2006a,Baker2006}.

In order to simulate the time evolution of a system regulated by Einstein's equations, one must first re-write them in a so called 3+1 formulation, separating a time coordinate, along which the system is evolved, from the spatial coordinates which label the spatial grid at each time step. The 3+1 formalism was originally indtroduced by Darmois (1927), Lichnerowitz (1944) and Choquet-Bruhat (1952). It was then used by Arnowitt, Deser and Misner for developping an Hamiltonian formulation of general relativity, known as the ADM formulation \cite{Arnowitt2008}.

The first step is to foliate the spacetime in spacelike hypersurfaces $\Sigma_t$, which are level functions of a scalar field $t$ (the time coordinate). The timelike, future-directed unit normal to each surface $n$ is linked with the gradient of $t$ by the relation
\begin{equation}
n^{\mu} := -\alpha \nabla^{\mu} t \label{eq:alpha}.
\end{equation}
This equation is the definition of the lapse function $\alpha$, which is given by $\alpha = -\sqrt{\nabla^{\mu}t \nabla_{\mu} t}$, to ensure that the vector $n^{\mu}$ has norm $n^{\mu} n_{\mu} = -1 $. In the previous expressions the spacetime metric $g_{\mu\nu}$ is implicitly used to lower the vector indexes, and to define the operator
\begin{equation}
\nabla_{\mu}\ =\ g_{\mu\nu} \nabla^{\nu}.
\end{equation} 
It is possible to select a privileged observer , denominated the Eulerian observer, which is the one with four-velocity $n^{\mu}$. This means its evolution is  normal to $\Sigma_t$, which, therefore, can be viewed as the set of events which are simultaneous for the Eulerian observer. 
From $\alpha$, one can also define the normal evolution vector $m^{\mu} := \alpha n^{\mu}$. It is characterized by:
\begin{equation}
\nabla_m t = m^{\mu}\nabla_{\mu} t = 1. \label{eq:m}
\end{equation} 
This means that the vector $m^{\mu} \delta t$ carries the hypersurface $\Sigma_t$ into $\Sigma_{t+\delta t}$.
Finally, one can define a spatial metric on each hypersurface
\begin{equation}
\gamma_{\mu\nu} := g_{\mu\nu} + n_{\mu} n_{\nu}.
\end{equation}

The next step is to decompose Einstein's equations into a spatial part (on each hypersurface) and a component orthogonal to the foliation. This is done with the projection operators:
\begin{align}
\gamma^{\mu}_{\nu} &= \delta^{\mu}_{\nu} + n^{\mu} n_{\nu}\\
\alpha^{\mu}_{\nu} &= -n^{\mu} n_{\nu},
\end{align}
where $\delta_{\mu}^{\nu}$ is the Euclidean metric.

After some computations, it can be shown that the spatial projection of the four dimensional Ricci tensor is:
\begin{equation}
\gamma^{\mu}_{\alpha} \gamma^{\nu}_{\beta} {}^{(4)}R_{\mu\nu} = -\frac{1}{\alpha} \mathcal{L}_{\mathbf{m}} K_{\alpha \beta} - \frac{1}{\alpha} D_{\alpha} D_{\beta} \alpha + R_{\alpha \beta} + K K_{\alpha \beta} - 2 K_{\alpha\mu} K^{\mu}_{\beta}, \label{eq:Ricci}
\end{equation}
where $R_{\alpha\beta}$ is the spatial Ricci tensor on $\Sigma_t$, $D_{\mu}$ is the spatial covariant derivative, computed with the metric $\gamma_{\mu\nu}$, and $K$ is the trace of the extrinsic curvature $K_{\mu\nu}$, which is defined by:
\begin{equation}
K_{\mu\nu} = -\gamma^{\mu}_{\alpha} \gamma^{\nu}_{\beta} \nabla_{(\mu} n_{\nu)} = -\frac{1}{2} \mathcal{\mathbf{n}} \gamma_{\mu\nu}, \label{eq:def_K}
\end{equation}
where the round brackets indicate a symmetrization respect to the indexes they contain.
It can be either seen as the projection on $\Sigma_t$ of the gradient of its unit normal, or the rate of change of the spatial metric. The projections of the energy-momentum tensor, instead, are:
\begin{align}
S_{\mu\nu} &= \gamma^{\alpha}_{\mu} \gamma^{\beta}_{\nu} T_{\alpha \beta}\\
e &= n^{\mu} n^{\nu} T_{\mu\nu}\\
j_{\mu} &= -\gamma^{\alpha}_{\nu}n^{\beta}T_{\alpha\beta},
\end{align}
where $e$ can be interpreted as the energy density and $j_{\mu}$ as the the momentum density measured by the Eulerian observer.

The final step to write the full set of ADM equations is to introduce a coordinate system, adapted to the foliation, in order to write the previous tensorial expressions as a system of PDEs. One first introduces coordinates $x^{i}$, $i\in [1,3]$ on each hypersurface $\Sigma_t$. Then, a coordinate $t$ is defined, such as the vector $\partial_{t}$ is tangent to the lines of constant spatial coordinates. This defined vector $\partial_t$ is dual to the 1-form $dt$, just like the normal evolution vector $\mathbf{m}$, defined before in \cref{eq:m}, so like $\mathbf{m}$ drags the hypersurfaces $\Sigma_t$. But $m^{\mu}$ and $\partial_{t}$ in general differ, and their difference is defined as the shift vector $\mathbf{\beta}$:
\begin{equation}
\beta^{\mu} := \partial_{t}^{\mu} - m^{\mu} \label{eq:beta}.
\end{equation}   
From this definition of $\beta$ and the definition of $m$ of \cref{eq:m}, one can derive
\begin{equation}
\partial_t^{\mu} = -\alpha n^{\mu} + \beta^{\mu}, 
\end{equation}
and, for the Lie derivatives,
\begin{equation}
\mathcal{L}_m = \mathcal{L}_{\partial_t} - \mathcal{L}_{\beta}. \label{eq:lie_beta}
\end{equation}
One can also express the components of the normal unit vector $n^{\mu}$ and its dual one-form $n_{\mu}$ in terms of the lapse and the shift:
\begin{align}
n^{\mu} &= \rnd{\frac{1}{\alpha},-\frac{\beta^i}{\alpha}}\\
n_{\mu} &= \rnd{-\alpha, \vec{0}}.
\end{align}
This allows to decompose the four-dimensional metric $g_{\mu\nu}$ and write it respect to the just defined coordinate system:
\begin{equation}
ds^2 = g_{\mu\nu} dx^{\mu} dx^{\nu} = -\rnd{\alpha^2 -\beta^i\beta_i} dt^2 + 2\beta_i dx^i dt + \gamma_{ij} dx^i dx^j. \label{eq:metric}
\end{equation}

The physical interpretation of the lapse function and the shift vector is the following: the lapse represents a measure of the proper time (for an Eulerian observer) between two adjacent hypersurfaces:
\begin{equation}
d\tau^2 = -\alpha^2(t,x^i) dt^2;
\end{equation}
while the shift vector represents the relationship between spatial coordinates of two adjacent hypersurfaces:
\begin{equation}
x^i_{t+\delta t} = x^i_t - \beta^{i}(t,x^i) dt.
\end{equation}
They characterize the choice of the coordinate system for the time evolution, and as such, in general relativity they are gauge variables whose evolution can be freely chosen. This choice is very important, as it is discussed in the next subsection, to guarantee numerical stability, in particular for simulations involving black holes.

Now it is finally possible to derive the full set of ADM equations, projecting the Einstein's equations and writing them in the chosen coordinate system. Projecting them twice in the direction normal to $\Sigma_t$ one obtains, after some algebric manipulation:
\begin{equation}
R + K^2 - K_{ij} K^{ij} - 16\pi e = 0, \label{eq:hamiltonian}
\end{equation}
where $R = g_{\mu\nu} R^{\mu\nu}$ is the curvature scalar. Equation \ref{eq:hamiltonian} is the so called \textit{Hamiltonian constraint}. Similarly, making a mixed space-time projection, one obtains:
\begin{equation}
D_j K^j_i - D_i K - 8\pi j_i = 0, \label{eq:momentum}
\end{equation}
which are the \textit{Momentum constraints}. These four constraints are elliptic equations, valid in each hypersurface $\Sigma_t$. In the numerical scheme implemented in \codename{The Einstein Toolkit}, they are not solved during the numerical evolution, but, instead, one should carefully monitor the constraints violations at each time step due to numerical errors. A high constraint violation means that the physical state of the system computed by the numerical code is not a solution of Einstein's equation, therefore it's not a physical state.

Projecting the Einstein's equations two times on the hypersurface $\Sigma_t$, instead, one obtains, starting from the expression of \cref{eq:Ricci} for the projected Ricci tensor and writing the equations as $R_{\mu\nu} = 8\pi \rnd{T_{\mu\nu} - \frac{1}{2}g_{\mu\nu}T}$, where $T$ is the trace of the energy-momentum tensor, an evolution equation for the extrinsic curvature:
\begin{align}
\partial_t K_{ij} &= -D_i D_j \alpha + \alpha \rnd{R_{ij} - 2 K_{ik} K^{kj} + K K_{ij}} + \label{eq:evol_K}\\
&- 8\pi \alpha \rnd{R_{ij} -\frac{1}{2}\gamma_{ij} \rnd{S-e}} + \mathcal{L}_{\beta} K_{ij},\notag
\end{align}
which describes how the spacetime geometry changes going from one hypersurface to the future adjacent one. These three equations (six evolution equations for the extrinsic curvature and 4 constraint equations) should be supplemented by the relationship between the spatial metric and the extrinsic curvature, which, using \cref{eq:lie_beta} and \cref{eq:def_K}, can be written as:
\begin{equation}
\partial_t \gamma_{ij} = -2\alpha K_{ij} + \mathcal{L}_{\beta} \gamma_{ij}. \label{eq:evol_gamma}
\end{equation}
\subsection{The BSSN formulation}
\label{sec:BSSN}
Unfortunately ADM equations (eq. \ref{eq:evol_K} and \ref{eq:evol_gamma}, with the constraints \ref{eq:hamiltonian} and \ref{eq:momentum}) can not be used in practice for GR numerical simulations, since they are only weekly hyperbolic, and would quickly lead to numerical instabilities causing our codes to crash. In the late 1990s and first 2000s a new formulation of the Einstein's equations was developed by several groups, called today the BSSN-OK formulation, from the initials of its main developers (Baumgarte, Shapiro, Shibata, Nakamura, Oohara and Kojima) \cite{Nakamura1987,Shibata1995,Baumgarte1998} \footnote{The contribution of the last two authors (Oohara and Kojima) is often forgotten, and the formalism is usually simply referred at as BSSN in numerical relativity publications.}. 

Instead of evolving the spatial metric $\gamma_{ij}$ and the extrinsic curvature $K_{ij}$, new variables can be introduced and evolved separately. First, a conformal spatial metric is constructed:
\begin{equation}
\tilde{\gamma}_{ij} := e^{-4\phi} \gamma_{ij},
\end{equation}  
with the conformal factor 
\begin{equation}
\phi := log\rnd{\frac{1}{12} det\rnd{\gamma_{ij}}}
\end{equation}
promoted to evolved variable. Then the conformal, trace-free extrinsic curvature is defined (and evolved):
\begin{equation}
\tilde{A}_{ij} := e^{-4\pi}\rnd{K_{ij}-\frac{1}{3}g_{ij} K},
\end{equation}
with $K = g^{ij}K_{ij}$. The indexes of $\tilde{A}_{ij}$ are raised or lowered with the conformal metric $\tilde{\gamma}_{ij}$. Finally, the conformal connection functions are introduced:
\begin{equation}
\tilde{\Gamma}^i := \tilde{\gamma}^{jk}\tilde{\Gamma}^i_{jk} = \partial_j-\tilde{\gamma}^{ij},
\end{equation}
where $\tilde{\Gamma}^i_{jk}$ are the Christoffel symbols of $\tilde{\gamma}_{ij}$ and the last equation comes from the fact that the conformal metric has determinant equal to one. Separating the trace part and the trace-free part of the ADM evolution equations \ref{eq:evol_K} and \ref{eq:evol_gamma}, one finally obtains the full BSSN system:
\begin{align}
\rnd{\partial_t - \beta^j \partial_j} K &= -\gamma^{ij} \tilde{D}_i \tilde{D}_j \alpha + \alpha \rnd{\tilde{A}^{ij} \tilde{A}_{ij} + \frac{1}{3} K^2} + 4\pi \rnd{e + \gamma^{ij} S_{ij}}\\ \label{eq:BSSN}
\rnd{\partial_t - \beta^j \partial_j} \phi &= -\frac{1}{6}\rnd{\alpha K - \partial_k \beta^k}\\
\rnd{\partial_t - \beta^j \partial_j} \tilde{\Gamma}^i &= -2 \tilde{A}^{ij}\partial_j \alpha +2\alpha \sq{\tilde{\Gamma}^i_{kl} \tilde{A}^{kl} + 6\tilde{A}^{ij}\partial_j \phi -\frac{2}{3} \tilde{\gamma}^{ij} \partial_j K}\\
&- \tilde{\Gamma}^j\partial_j \beta^i + \frac{2}{3} \tilde{\Gamma}^i \partial_j \beta^j + \frac{1}{e} \tilde{\gamma}^{ik} \partial_j \partial_k \beta^j + \tilde{\gamma}^{jk} \partial_j \partial_k \beta^i -16 \pi \alpha \tilde{\gamma}^{ik} j_k \notag \\
\rnd{\partial_t - \beta^j \partial_j} \tilde{\gamma}_{ij} &= -2\alpha\tilde{A}_{ij} + 2 
\tilde{\gamma}_{k(i} \partial_{j)} \beta^k - \frac{2}{3} \tilde{\gamma}_{ij} \partial_k \beta^k\\
\rnd{\partial_t - \beta^j \partial_j} \tilde{A}_{ij} &= e^{-4\pi} \sq{\alpha \tilde{R}_{ij}^{\phi} - \tilde{D}_i \tilde{i}_j \alpha}^{TF} + \\
&+ \alpha K \tilde{A}_{ij} - 2 \alpha \tilde{A}_{ki} \tilde{A}^k_j + 2 \tilde{A}_{k(i}\partial_{j)} \beta^k - \frac{2}{3} \tilde{A}_{ij} \partial_k \beta^k -8\pi \alpha e^{-4\pi} S_{ij}^{TF} \notag, \label{eq:BSSN}
\end{align}
where TF stands for \quotes{trace-free}, $\tilde{D}_i$ is the covariant derivative associated with the conformal spatial metric and the Ricci tensor has been divided in two components, one containing the terms with derivatives of the conformal metric and its Christoffel symbols ($\tilde{R}_{ij}$), and the other containing only the terms with covariant derivatives of the conformal factor ($R_{ij}^{\phi}$). In deriving these equations, the Hamiltonian and momentum constraint relationships have been used. In particular, it is key to use the momentum constraint to eliminate the divergence of $\tilde{A}_{ij}$ from the evolution equations for the conformal connection $\tilde{\Gamma}^i$, to ensure numerical stability. This system of 17 PDEs was rigorously proven to be strongly hyperbolic in ref. \cite{Sarbach2002,Nagy2004}, even if it is not easy to see it at glance. It has introduced, however, five additional constraints which should be verified at each evolution step:
\begin{align}
&det\rnd{\tilde{\gamma}_{ij}} = 1\\
&Tr\rnd{\tilde{A}_{ij}} = 0\\
&\tilde{\Gamma}^i = \tilde{\gamma}^{jk}\tilde{\Gamma}^i_{jk}.
\end{align}
The first two constraints are actively enforced by the code at each time step. The third one, instead, is not enforced (like the Hamiltonian and momentum constraints of eq. \ref{eq:hamiltonian} and \ref{eq:momentum}). However, to help keeping its violation low, the evolved $\tilde{\Gamma}^i$ are used only where their derivatives are needed, but where they are needed without any derivative, $\tilde{\gamma}^{jk}\tilde{\Gamma}^i_{jk}$ is used, instead.

This evolution system must be supplemented with a choice for the gauge variables $\alpha(t,x^i)$ and $\beta^j(t,x^i)$. A simplistic choice, like the geodesic slicing ($\alpha = 1$ and $\beta^i = 0$) does not work, and leads to code crashes when evolving spacetimes containing black holes. This happens because it is not singularity avoiding, therefore, starting a simulation from BH initial data with a future singularity, that singularity is reached quickly in the numerical evolution. There is, therefore, a need for a  singularity-avoiding slice condition on $\alpha$. The lapse can not be constant everywhere, but should approach the Minkowski value of 1 in the asymptotic flat region near the grid outer boundaries and should become close to zero in the vicinity of a black hole, to avoid reaching the singularity. This causes the additional problem that, because time advances \quotes{faster} in the far region and is almost frozen near the BH, the grid (or, more precisely, each hypersurface $\Sigma_t$) gets strongly distorted (this phenomenon is known as grid stretching), with grid points falling inside the black hole horizon, even if points inside it are excised from the numerical evolution. This causes the code to crash, due to the large gradients in the metric functions. To fix this problem, one needs to set also a spatial gauge condition for the shift vector $\beta^i$, which should also change in space and time. 

One common solution for the lapse is the \textit{maximal slicing} condition $K=0$, or the related \textit{K-freezing} condition $\partial_t K = 0$ (equivalent in the case of $K=0$ set in the initial data). The K-freezing condition, however, leads to the need of solving an elliptic equation for the lapse at each time step, which is computationally expensive:
\begin{equation}
\Delta \alpha = \beta^i \partial_i K + \alpha K_{ij} K^{ij}.
\end{equation}
An efficient alternative solution, is to contruct an hyperbolic slicing condition, called the \textit{K-driver} condition, making $\partial^2_t \alpha$ proportional to $\partial_t K$ \cite{Bona1995,Alcubierre2003}:
\begin{equation}
\partial_t \alpha - \beta^i \partial_i \alpha = -2\alpha K.\label{eq:evol_alpha}
\end{equation}
With this particular choice of parameters, this condition takes the name of \quotes{1+log} slicing.

As spatial gauge condition, to avoid grid stretching one can, in the same way, impose the \textit{Gamma-freezing} condition $\partial_t \tilde{\Gamma}^i = 0$, which implies solving elliptic equations for $\beta^i$, or impose it dynamically with the hyperbolic \textit{Gamma-driver} condition, as implemented in the Einstein Toolkit:
\begin{align}
\partial_t \beta^i - \beta_j \partial^j \beta^i = \frac{3}{4} B^i \label{eq:evol_beta}\\
\partial_t B^i - \beta_j \partial^j \beta^i = \partial_t \tilde{\Gamma}_i - \eta B^i,
\end{align} 
where the additional evolved variable $B^i$ has been introduced. The \quotes{Beta driver} $\eta$ is a parameter which acts as a damping factor, driving $B^i$ (and then $\partial_t \beta^i$) to zero, so the shift vector will tend to a constant in a stationary spacetime (in other words, this is a symmetry-seeking gauge condition). Being a damping factor with dimensions $\sq{1/T}$, it sets, depending on the numerical integration method, a Courant-like limitation for the time step size, but independent from the spatial resolution. Since in FMR simulations with subcycling in time $\Delta t $ is much larger near the grid boundaries than in the interior, a constant choice for the $\eta$ parameter can lead to numerical instabilities in the coarser grid \cite{Schnetter2010}. For this reason, a simple space-varying prescription for $\eta$ is implemented in the Einstein Toolkit:
\begin{eqnarray}
  \label{eq:varying-simple}
  \eta(r) & := & \frac{2}{M_{TOT}}\; \left\{
    \begin{array}{llll}
      1 & \mathrm{for} & r \le R & \textrm{(near the origin)}
      \\
      \frac{R}{r} & \mathrm{for} & r \ge R & \textrm{(far away)}
    \end{array}
    \right. ,
\end{eqnarray} 
where $r$ is the distance from the coordinates origin and $M_{TOT}$ is the total gravitational mass of the simulated system.

These BSSN-OK equations are implemented in the Einstein Toolkit in the \codename{McLachlan} thorn \cite{Brown2009,Loffler2012}, auto-generated as highly efficient vectorized code from a Mathematica script written in tensorial notation with \codename{Kranc} \cite{Husa2006}. The spatial partial derivatives in the right hand sides of equations are computed with a fourth order finite differencing representation: given $f_n$ the value of a function $f$ in the n-th grid point in the x-direction, for example, its derivative is expressed as:
\begin{equation}
\partial_x f = \frac{1}{12} f_{n-2} -\frac{2}{3} f_{n-1} +\frac{2}{3} f_{n+1} - \frac{1}{12} f_{n+2}.
\end{equation}  
The advection terms $\beta^i \partial_i$, instead, are computed using a fourth order upwind scheme, instead of a centred differences, to have a better numerical stability. Finally, Kreiss-Oliger fifth-order dissipation \cite{Gustafsson2013} is applied to the evolved curvature variables, with a strength of $\epsilon_{diss} = 0.01$, in order to damp high-frequency oscillations.

The BSSN-OK system of evolution equations must be supplemented by suitable boundary conditions at the external edges of the computational grid. This is a non-trivial problem in numerical relativity, because one wants to obtain a stable evolution at the boundary, with constraints preservation, and more importantly, one wants that gravitational waves will propagate out of the grit and will not be reflected back inside. Or, stated differently, one wants to impose a no-incoming-radiation boundary condition. In the Einstein Toolkit this is done in an approximated way adopting Sommerfeld-type radiative boundary conditions \cite{Alcubierre2000}: for each evolved variable tensor component $X$, the main part of the boundary condition looks like
\begin{equation}
X = X_0 + \frac{u\rnd{r-v_0t}}{r},
\end{equation}  
behaving like an outgoing radial wave with speed $v_0$. Here $u$ is a spherically symmetric perturbation and $X_0$ the component value at spatial infinity. This assumes that the waves at the boundary have a spherical wavefront, which is true if the boundary is far enough from the source. In practice, this condition is implemented deriving it in time and obtaining the following differential equation, which is easier to impose in a finite-difference code:
\begin{equation}
\partial_t X = -\frac{v_0 x^i}{r}\partial_i X - v_o\frac{X-X_0}{r}.
\end{equation}
The spatial derivatives are computed with second order central finite differencing when possible, and with one-sided finite differencing otherwise.
To account also for those parts in the solution which does not behave like a pure outgoing wave, the time derivative term $\partial_t X$ is modified with:
\begin{equation}
\rnd{\partial_i X}^* = \partial_t X + \rnd{\frac{r}{r-n^i\partial_i r}}^p n^i\partial_i \rnd{\partial_t X},
\end{equation}
where $n^i$ is the unit normal vector of the considered boundary face, and this correction decays with a power $p=2$ of the radius in our simulations. These conditions are implemented in The Einstein Toolkit by the thorn \codename{newrad}. 
\subsection{Z4 family formulations}
\label{sec:Z4}
An always-present concern during GR numerical simulations with BSSN-OK are the constraint violations either present in the initial data or developed during the numerical evolution. To alleviate this problem, a new formulation of the Einstein equations, the $Z4$ formulation, started being adapted for numerical relativity by different groups \cite{Bona2003,Bona2004a,Gundlach2005,Bernuzzi2010,Alic2012}. The $Z4$ formulation can be derived from the following Lagrangian, with a Palatini-type variational principle \cite{Bona2010}
\begin{equation}
L = g^{\mu\nu}\rnd{R_{\mu\nu} + 2 \nabla_{\mu} Z_{\mu}}.
\end{equation}
It possesses an extra vector term $Z_{\mu}$ in addition to the standard Hilbert action. This vector measures the distance of a solution from the Einstein equations, and the algebraic constraint $Z_{\mu} = 0$ takes the place of Hamiltonian (\ref{eq:hamiltonian}) and momentum (\ref{eq:momentum}) constraints of the ADM formulation. In order to damp the constraints violation, extra terms can be introduced, to make Einstein's equation solutions an attractor for the full $Z4$ system solutions. In covariant form, the $Z4$ system with constraint damping is:
\begin{align}
R_{\mu\nu} &+\nabla_{\mu} Z_{\nu} + \nabla_{\nu} Z_{\mu} +k_1 \sq{n_{\mu} Z_{\nu} + n_{\nu} Z_{\mu} - (1+k_2) g_{\mu\nu} n_{\rho} Z^{\rho}} = \\
& = 8\pi \rnd{T_{\mu\nu} - \frac{1}{2}g_{\mu\nu} T}. \notag
\end{align}
The coefficients $k_i$ are linked to the characteristic times of the constraint violation decay. This system, as the original Einstein's equations, can be written in 3+1 form, and, then, it is possible to develop a conformal decomposition, similar to the one of BSSN-OK. Two different conformal Z4 formulations have been studied: the \codename{CCZ4} formulation \cite{Alic2012,Alic2013}, which retains the full equations and in this way is a covariant formulation, at the price of greater differences respect to BSSN-OK and the need to introduce an additional parameter $k_3$ to be able to evolve also spacetimes containing black holes and the \codename{Z4c} \cite{Gundlach2005,Bernuzzi2010,Hilditch2013} formulation, which  retains only the principal part of the equations, in order to be as close as possible to BSSN-OK, but is not fully covariant and needs the introduction of ad-hoc constraint preserving boundary conditions in order to avoid constraint violations being reflected back inside the grid at the outer boundaries \cite{Ruiz2011}. Since in \codename{McLachlan} only the CCZ4 formulation is implemented, I will describe that in the following paragraphs.

After the standard BSSN-OK conformal decomposition, and the separation of traceless and trace parts, the CCZ4 systems is the following, where I have marked in red the terms added respect to standard BSSN-OK (\ref{eq:BSSN}), and in blue the ones added for constraints violation damping:
\begin{align}
\rnd{\partial_t - \beta^j \partial_j} K &= -\gamma^{ij} \tilde{D}_i \tilde{D}_j \alpha + \alpha \rnd{R + K^2 + \mathcolor{red}{2D_i Z^i -2\Theta K}} + \label{eq:CCZ4}\\
&- \mathcolor{blue}{3\alpha k_1 (1+k_2) \Theta} + 4\pi \rnd{e + \gamma^{ij} S_{ij}} \notag\\ 
\rnd{\partial_t - \beta^j \partial_j} \phi &= -\frac{1}{6}\rnd{\alpha K - \partial_k \beta^k}\\
\rnd{\partial_t - \beta^j \partial_j} \hat{\Gamma}^i &= -2 \tilde{A}^{ij}\partial_j \alpha +2\alpha \sq{\tilde{\Gamma}^i_{kl} \tilde{A}^{kl} + 6\tilde{A}^{ij}\partial_j \phi -\frac{2}{3} \tilde{\gamma}^{ij} \partial_j K} +\\
&+ 2 \tilde{\gamma}^{ki}\rnd{\mathcolor{red}{\alpha \partial_k \Theta - \Theta \partial_k \alpha - \frac{2}{3} \alpha K Z_k}} + \mathcolor{blue}{2 k_3 \rnd{\frac{2}{3}\tilde{\gamma}^{ij} Z_j \partial_k \beta^k - \tilde{\gamma}^{jk}Z_j \partial_k \beta^i}} + \notag\\ 
&- \tilde{\Gamma}^j\partial_j \beta^i + \frac{2}{3} \tilde{\Gamma}^i \partial_j \beta^j + \frac{1}{e} \tilde{\gamma}^{ik} \partial_j \partial_k \beta^j + \tilde{\gamma}^{jk} \partial_j \partial_k \beta^i -16 \pi \alpha \tilde{\gamma}^{ik} j_k \notag \\
\rnd{\partial_t - \beta^j \partial_j} \tilde{\gamma}_{ij} &= -2\alpha\tilde{A}_{ij} + 2 
\tilde{\gamma}_{k(i} \partial_{j)} \beta^k - \frac{2}{3} \tilde{\gamma}_{ij} \partial_k \beta^k\\
\rnd{\partial_t - \beta^j \partial_j} \tilde{A}_{ij} &= e^{-4\pi} \sq{\alpha \tilde{R}_{ij}^{\phi} - \tilde{D}_i \tilde{i}_j \alpha \mathcolor{red}{+D_i Z_j + D_j Z_i}}^{TF} + \\
&+ \alpha \tilde{A}_{ij} \rnd{K - \mathcolor{red}{2\Theta}} - 2 \alpha \tilde{A}_{ki} \tilde{A}^k_j + 2 \tilde{A}_{k(i}\partial_{j)} \beta^k - \frac{2}{3} \tilde{A}_{ij} \partial_k \beta^k -8\pi \alpha e^{-4\pi} S_{ij}^{TF} \notag\\
\mathcolor{red}{\rnd{\partial_t -\beta^j \partial_j }\Theta} &= \mathcolor{red}{\frac{1}{2}\alpha \rnd{R + 2D_iZ^i - \tilde{A}_{ij} \tilde{A}^{ij} + \frac{2}{3} K^2 -2\Theta K} - Z^i\partial_i \alpha}  +\\
&\mathcolor{blue}{-\alpha k_1 (2+k_2)\Theta} \mathcolor{red}{-8\pi\alpha e}. \notag
\end{align}
The four-vector $Z^{\mu}$ have been separated in $Z_i$ and $\Theta = Z_0$. The conformal connections has been substituted by $\hat{\Gamma}^i := \tilde{\Gamma}^i +2\tilde{\gamma^{ij}Z_j}$. In this particular implementation, in the equation for the trace of the extrinsic curvature $K$ the Ricci scalar is not substituted using the Hamiltonian constraint. 
This is done, instead, in the Z4c formulation. The 1+log and Gamma-freezing gauge conditions must also be slightly modified, to account for the new variables:
\begin{align}
\partial_t \alpha - \beta_j \partial^j \alpha &= -2\alpha \rnd{K \mathcolor{red}{-2\Theta}}\\
\partial_t \beta^i - \beta_j \partial^j \beta^i &= \frac{3}{4} B^i\\
\partial_t B^i - \beta_j \partial^j B^i &= \partial_t \hat{\Gamma}_i - \beta_k \partial^k \hat{\Gamma}^i - \eta B^i
\end{align}
The term with $k_3$ has been added in \cite{Alic2012} to alleviate numerical instabilities in evolving black holes: a value $k_3 = 1$ is used to keep full covariance, while a value $k_3 = \frac{1}{2}$ makes the black hole evolution possible. This is not necessary, instead, in the Z4c formulation.

The Z4 family formulations bring two main advantages over BSSN-OK: first, it is possible to define a natural constraint-damping scheme for every constraint, which is not the case for BSSN. This is useful, for example, when evolving constraint-violating initial data, as done for example in \cite{Kastaun2015} for evolving spinning binary neutron stars. The constraint violations get dumped quickly by at least a couple of orders of magnitude respect to BSSN-OK. 
Second, the constraint subsystem in BSSN-OK has a characteristic with speed 0. This means that constraint violations remain on the numerical grid where they are generated by numerical errors, and do not propagate. This is a problem when evolving a physical system for which the interesting parts are always at the same grid locations, like a single star or black hole, for example after the merger of a BNS system. In the Z4 formulation, instead, all the constraint characteristics have a speed of $\pm 1$. This means that the generated constraint violations propagate out of the numerical grid with the speed of light.

One disadvantage of the CCZ4 formulation, already discussed in \cite{Alic2012}, and confirmed by our simulations (see ref. \cite{DePietri2016} and \cref{sec:convergence}) is that a higher resolution is needed to enter in the convergent regime, respect to BSSN-OK, because the Hamiltonian constraint, even if damped, enters directly in the evolution equations, while is assumed to be fully satisfied and as such put to zero in the BSSN-OK equations. Another possible problem of all Z4-family formulations is the choice of a correct value for the damping parameters. In our simulations, following \cite{Alic2013}, we used $k_1 = 0.05$, $k_2 = 0$ and $k_3 = 1$. 