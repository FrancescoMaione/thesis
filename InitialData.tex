\section{Initial data computation}
\label{sec:initial_data}
Generating accurate initial data for binary neutron stars in quasi-circular orbits is a non-trivial first step, necessary for the fully dynamical evolution. In fact, the results of this work point to the presence of multiple errors in simulation results linked with the initial data computation technique (see sec. \ref{sec:e} and \ref{sec:d}). For this thesis was used the only public code for BNS initial data, the \codename{LORENE} library \cite{Bonazzola1999,Taniguchi2001}.

BNS prior to the merger phase are believed to be in circular orbits, thanks to the circularization properties of the emitted gravitational radiation (see the predicted eccentricity values at $10Hz$  for the observed BNS binaries in table \ref{tab:dns}). Additionally, they will most likely be in an irrotational state, because the viscous forces are too weak and act on a time-scale too long respect to the gravitational radiation one in order to be able to successfully synchronize the stars spins with the orbital rotation. Initial data computation starts, therefore, assuming the existence of an \textit{helicoidal} Killing vector, which, for an asymptotic inertial observer at rest respect to the binary, takes the form
\begin{equation}
I = \pd{}{t} + \Omega \pd{}{\phi}. \label{eq:helicoidal}
\end{equation}   
This assumption has important consequences, for example it implies ignoring the outgoing gravitational radiation, and its backreaction on the binary dynamics (since this radiation-reaction will enter in the post-Newtonian expansion at the 2.5PN order, this helicoidal symmetry can be exact up to 2PN). This is responsible for the lack of a radial component of the star velocity (which comes from radiation reaction in the real system) and consequently for the small orbital eccentricity once these initial data are evolved (see sec. \ref{sec:e}). Another consequence of the helicoidal symmetry is that it leads to a non-asymptotically flat spacetime. Appropriate approximations should therefore be done in the curvature evolution to avoid diverging metric coefficients at infinity. In the \codename{LORENE} code (and in most of the private code developed by different groups in the last years, for example \cite{Dietrich2015a,Tsokaros2015}) the so called \textit{Conformal Thin Sandwich} approach is used, based on the Wilson and Mattews scheme \cite{Wilson1989}. In a 3+1 splitting like presented in sec. \ref{sec:adm}, the spatial metric is approximated as conformally flat:
\begin{equation}
\gamma_{ij} = A^2 \eta_{ij}.
\end{equation}
This approximation is justified because it would be exact for spherically-symmetric spacetimes, it is a very good and commonly used approximation for constructing axisymmetric isolated neutron stars initial data, and it's a good description of the BNS spacetime when the stars are far apart. To further simplify the computations, one can build a coordinate frame compatible with the helicoidal symmetry, where $I = \pd{}{t}$. Defining the corotating shift 
\begin{equation}
\omega^i = \beta^i + \Omega \pd{}{\phi},
\end{equation}
and using the Killing equation 
\begin{equation}
\nabla_{\mu}I_{\nu} + \nabla{\nu}I_{\mu} = 0
\end{equation}
one can construct a relation for the extrinsic curvature $K^{ij}$:
\begin{align}
K^{ij} &= -\frac{1}{2\alpha}\rnd{\nabla^i \beta^j + \nabla^j \beta^i} =\\
&= -\frac{1}{2A^2\alpha}\rnd{\partial^i\omega^j+\partial^j\omega^i -\frac{2}{3}\eta^{ij}\partial_k \omega^k}.\notag
\end{align}
This relationship, thanks to the helicoidal symmetry, fixes all spatial components of the extrinsic curvature, given the corotating shift. This means fixing eq. \ref{eq:evol_K} of the ADM formulation, leaving a free choice only for its trace $K$, which is fixed imposing a maximal slicing gauge condition ($K = \partial_t K = 0$, see also sec. \ref{sec:BSSN}). Taking into account the Hamiltonian and momentum constraints and the trace of the Einstein equations, one gets five elliptic equations to be solved for the variables $\omega^i$, $A$, and $\alpha$. In the \codename{LORENE} code, the following alternative variables are defined:
\begin{align}
\nu &:= ln(\alpha)\\
\beta &:= ln(\alpha A),
\end{align} 
to get, finally, the following equations:
\begin{align}
&\nabla^2 \nu = 4\pi A^2 (e+S) + A^2 K^{ij}K_{ij} - \partial_i \nu \partial^i \beta \label{eq:AID}\\
&\nabla^2 \beta = 4\pi A^2 S + \frac{3}{4}A^2 K_{ij}K^{ij} -\frac{1}{2}\rnd{\partial_i\nu \partial^i \nu + \partial_i \beta \partial^i \beta}\label{eq:alphaID}\\
&\nabla^2 \omega^i + \frac{1}{3}\partial^i \partial_j \omega^j &= -16\pi \alpha A^2 j^i + 2\alpha A^2 K^{ij}\partial_j\rnd{3\beta - 4	\nu} \label{eq:betaID},
\end{align}
where all Laplacians are computed respect to the flat spatial metric.
The remaining five Einstein's equations are not considered in this scheme, and are probably violated, reflecting the fact that this is only an approximation of the true BNS spacetime.

For describing the initial hydrodynamics variables profiles, instead, it is easier to trade the energy-momentum tensor and baryon number conservation with the following \textit{uniformy canonical equations of motion}, if a recipe exists to obtain all thermodynamical variables from the specific enthalpy:
\begin{align}
u^{\mu}\rnd{\nabla \times \mathbf{w}}_{\mu\nu} = 0 \label{eq:motion1}\\
\nabla^{\mu}\rnd{\rho u_{\mu}} = 0, \label{eq:motion2}
\end{align}
where $w_{\mu} = h u_{\mu}$ is the comomentum 1-form, and its exterior derivative
\begin{equation}
\rnd{\nabla \times \mathbf{w}}_{\mu\nu} = \nabla_{\mu} w_{\nu} - \nabla_{\nu}w_{\mu}
\end{equation}
is the vorticity two-form. It is evident that a \textit{potential flow}
\begin{equation}
w_{\mu} = \nabla_{\mu} \psi,
\end{equation}
for which $\rnd{\nabla \times \mathbf{w}}_{\mu\nu} = 0$, is a solution to eq. \ref{eq:motion1}. This is the GR generalization of a Newtonian irrotational flow. From the helicoidal symmetry definition, one has $\mathcal{L}_I w = 0$. Using Cartan's identity, this becomes:
\begin{equation}
I^{\mu}\rnd{\nabla \times \mathbf{w}}_{\mu\nu} + \nabla^{\mu} \rnd{I^{\nu}w_{\nu}} = 0,
\end{equation}
which implies that 
\begin{equation}
I^{\nu}w_{\nu} = const. \label{eq:first_integral}
\end{equation}
is a constant of motion. Using this first integral, the fluid motion is completely determined by the potential $\psi$, which must still satisfy eq. \ref{eq:motion2}:
\begin{equation}
\frac{\rho}{h}\nabla^{mu}\nabla_{\mu}\psi + \nabla^{\mu}\psi \nabla_{\nu}\rnd{\frac{\rho}{h}} = 0.
\end{equation}
In the 3+1 approach, this equation becomes, on every hypersurface $\Sigma_t$, the following elliptic Poisson-like equation:
\begin{equation}
\rho \partial_i \partial^i \psi + \partial^i \rho \partial_i \psi = -\frac{hW}{\alpha}\beta^i\partial_i \rho + \rho\sq{\rnd{\partial^i \psi + \frac{hW}{\alpha}\beta^i} \partial_i ln(h) - \partial^i \psi \partial_i \alpha - \frac{\beta^i}{\alpha} \partial_i \rnd{hW}} + K\rho h W. \label{eq:matterID}
\end{equation}
To conclude, BNS initial data are computed solving equations \ref{eq:AID}, \ref{eq:alphaID}, \ref{eq:betaID}, \ref{eq:matterID}, with appropriate asimptotic flatness boundary conditions at large radii:
\begin{align}
\alpha \to 1\\
\psi \to 1\\
\omega^i \to \omega \times \vec{r}.
\end{align}
In \codename{LORENE} this is done with an iterative procedure, using a multidomain spectral method. In this method, different numerical grids are built around each star center, defined as the point with maximum specific hentalpy, which could not coincide with the Newtonian center of mass of the star. In Cartesian coordinates, each star is located on the $x$ axis, and they are equidistant from the coordinates center, with a distance between the stars centres $d$ fixed before the initial data computation (a comprehensive study of this initial distance impact on the simulation results can be found in \cite{Maione2016} and in sec. \ref{sec:d} of this thesis). During each iteration, the stars do not move respect to the grid center, even if they have a different mass. The rotation axis position is, instead, changed at every step. Generating our initial data, we used four coordinates domains for each star: the inner domain covers the star interior, it has a ball topology, its inner boundary coincides with the star center and its outer boundary with the star surface. Two domains, then, cover the exterior of each star, starting from its surface and reaching a finite radius, with a spherical shell topology. The last domain, instead, reaches the spatial infinity, to be able to set the right asymtotical flatness boundary conditions, and it has a compactified coordinate representation. The equations to be solved are discretized with a \textit{collocation spectral method}, meaning that the fields are expanded on a series of basis functions, and, in each domain, each field can be represented either by its spectral expansion coefficients or its values at particular grid points. In \codename{LORENE} Chebyshev polynomials are used as basis in the radial coordinate, trigonometrical polynomials or associated Legendre functions in $\theta$ and a Fourier series in $\phi$. In each domain, we decomposed a field with 33 points (or spectral coefficients) in the radial direction, 21 points in $\theta$ and 20 in $\phi$, following the suggestion of \cite{Taniguchi2001}.  

For finding the equation solutions, the metric potential are split in the so called \textit{autopotentials}, which are the potentials in a star domains generated by the same star
gravitational field, and the \textit{comp-potentials}, which are the potentials in a star domains generated by the companion field. In a typical code run, given as initial parameters the baryon mass of the two stars, their interbinary distance and their EOS, the first step (executed once) is to create two non-rotating equilibrium  spherical neutron star configurations at the initial positions. Their orbital angular velocity is set according to a second-order post-Newtonian expression. Next, the iterative procedure starts. At every iteration, first, the orbital angular velocity and the position of the rotation axis $x_c$ are computed, taking the gradient along the $x$ direction of the first integral of motion \ref{eq:first_integral}. Imposing that the hentalpy is, by definition, maximal at star centres, one obtains two equations:
\begin{equation}
\pd{}{x} \left. ln\rnd{W_0}\right|_{(x_{(1,2)},0,0)} = \pd{}{x}\left. \rnd{\nu + ln\rnd{W_u}}\right|_{(x_{(1,2)},0,0)},
\end{equation}
where $W_0 = -n^{\mu}v_{\mu}$, $W_u = -u^{\mu}v_{mu}$ and $x_{(1,2)}$ are the positions of the two stellar centres. All the variables in these equations can be expressed as function of $\Omega$ and $x_c$, and they can be solved using a simple zero-finding secant method. Some care must be taken when highly unequal mass configurations are considered. In this simple approach, which we used, it is important to properly set the initial interval for the zero-finding procedure: big enough such that it contains the true $x_c$ and $\Omega$, but small enough to ensure that the numerical procedure can converge to the right solution. We slightly modified the \codename{LORENE} code in order to more easily set and change those parameters. This was proven to be key in constructing initial data for unequal mass BNS, up to mass ratio values which have rarely been simulated in the past, also because of the difficulty in generating appropriate initial data for them. A different approach for solving the same problem is presented in ref. \cite{Taniguchi2010}, where $x_c$ is set in order to drive to zero the total linear momentum of the system, similarly of what is done for building BH-NS binaries initial data. After this first step, the hydrodynamics equation \ref{eq:matterID} is solved, using the specific hentalpy as variable. From $h$, new values of the thermodynamical quantities $\rho$, $\epsilon$, $p$, are computed using the EOS, and are used to build the energy-momentum tensor for solving the Einstein equations \ref{eq:alphaID}, \ref{eq:betaID} and \ref{eq:AID}. Before the beginning of a new iteration step, some relaxation is applied to the fields, to help insuring convergence. Moreover, the comp-potentials are not updated every code step, but every 8 steps. After the first 20 iteration, the code is also forced to converge to the right baryon mass for each star, multiplying the central specific entalpy of each star by a factor
\begin{equation}
\rnd{\frac{2+\xi}{2+2\xi}}^{1/4},
\end{equation}
where $\xi$ is the difference between the baryon mass value for the star in question at the present code iteration and its requested baryon mass at the end of the computation. The code iterations end when the relative difference in the central specific hentalpy between two steps is less than \num{1e-8}.