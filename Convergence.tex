\section{Convergence}
\label{sec:convergence}
Verifying the convergence of a numerical code is an essential part in any numerical simulation data analysis. However, in complex simulations like general relativistic binary neutron star mergers, it could be difficult to uniquely define the code convergence order, finding the most appropriate observables to compute it. The classical notion of convergence states that, taking simulations with different grid spacing $dx$, any observable $C$ should scale as 
\begin{equation}
C(dx) = C(dx = 0) + \varepsilon \rnd{dx}^p,\label{eq:conv1}
\end{equation}
where $C(dx = 0)$ represents the \textit{true value} of the observable, at infinite resolution, and the second term represents the finite-resolution error, which, in a finite difference code like ours, should have a polynomial dependence on the grid resolution. The easiest way to compute the convergence order, given three simulations at different resolutions $dx_{high},\ \Delta x_{medium}\ \mbox{and}\ \Delta x_{low}$, is to use the following relation (see, for example, ref. \cite{Reisswig2013a}):
\begin{equation}
\frac{\abs{C_{medium}\ -\ C_{low}}}{\abs{C_{high}\ -\ C_{medium}}}\ =\ \frac{\abs{dx_{medium}^p - dx _{low}^p}}{\abs{dx_{high}^p - dx_{medium}^p}}.\label{eq:3pconv}
\end{equation}
Using a zero-finding algorithm, such as the secant method, it is possible to extract the value of the convergence order $p$. This widely used technique has, however, the drawback that the result for $p$ often changes choosing three different resolutions for the analysis \cite{Bernuzzi2016}. It is also not possible to understand, just from three simulations, if the employed numerical algorithms are in the convergent regime, or which is the minimum resolution for which convergence at the nominal order is guaranteed. Another problem of grid-based codes convergence is that there could be some physical processes acting at low scales, which start to be detected only when the grid resolution is some times smaller the length scale of the effect one wants to observe. This is particularly troublesome in turbulent flows, like in the merger phase of binary neutron stars, where there is an inverse cascade of energy coming from smaller and smaller scales, as they are available  to the numerical simulation at increasingly better resolutions, destroying any classical knowledge of convergence. For this reason (and also for the absence shocks, excluding the stars surfaces) convergence analysis of BNS simulations are often done only for the inspiral phase, like in ref. \cite{Radice2014,Hotokezaka2015,DePietri2016,Bernuzzi2016}.

\subsection{Numerical methods comparison}
In ref. \cite{DePietri2016} were compared the convergence properties, in the inspiral phase, of different numerical methods combinations, changing, in particular, the hydrodynamical variables reconstruction method (between PPM, MP5 and WENO, see sec. \ref{sec:recontruction}), and the formulations adopted for the Einstein equations (BSSN (sec. \ref{sec:BSSN}) or CCZ4 (sec. \ref{sec:Z4})). The comparison were done for a single model SLy1.4vs1.4, with an initial separation between the stars of \SI{40}{km}. As observable the merger time was used, defined as the retarded time for which the amplitude of the GW emission is maximum. This global variable somewhat sums up all the effects in place during the inspiral.

\begin{figure}
\begin{center}
\includegraphics[]{figs/PRD_FitMergerTime14vs14Table.pdf}
\end{center}
\vspace{-5mm}
\caption{Fits of the merger time $t_\mathrm{merger}$ as a function of the resolution $dx$ using 
the PPM (dashed-dot black line) and the WENO (dashed red line) reconstruction methods,
and the BSSN-NOK evolution methods for the gravitational variable assuming second order 
convergence. In the case of WENO reconstruction we also report (thick gray line)
the fit were the convergence order $p$ is computed. In this case
the computed convergence order is $p=1.96\pm 0.14$. The values
of the merger time extrapolated to $dx=0$ are $10.39\pm0.03$ and $10.55\pm 0.20$
for WENO and PPM data, respectively. Please note that the two fits 
corresponding to the WENO data are so close that they are on top of each other here.
%%%\todo{FL: We miss a legend for the markers here.}
}\label{fig:meth_comp}
\end{figure}

Figure \ref{fig:meth_comp} shows the merger times of simulations with different combinations of the mentioned methods from resolution $dx = \SI{0.75}{\msun}$ to resolution $dx = \SI{0.185}{\msun}$. The same results are also reported in the next appendix in table \ref{tab:models3}. From these results, it is easy to conclude that the combination BSSN+WENO is the best for entering in the convergent regime even at very low resolutions. In particular, it is the only one for which even in simulations at resolution $dx = \SI{0.5}{\msun}$ and $dx = \SI{0.75}{\msun}$ the stars merge, leading to a qualitatively correct outcome, very useful to be able to run, even in regular workstations, many preliminary low-resolution simulations, in order to select the most interesting models to evolve later at higher resolution. With all the other methods, instead, the stars do not merge in simulations with resolution $dx \geq \SI{0.5}{\msun}$. One interesting thing to notice is that for all methods but BSSN+WENO the merger time decreases increasing the resolution. This difference, as explained also in ref. \cite{}, could be due to a different sign in the numerical errors introduced by different methods combinations, which lead to a different effect (in sign) on the resolution dependence of the merger time. Despite this difference, all three combinations with a merger time decreasing at better resolution (BSSN+PPM, BSSN+MP5, CCZ4+WENO) show compatible results for the merger time at $dx = \SI{0.25}{\msun}$, which was chosen as standard resolution for the simulations in \cite{DePietri2016,Maione2016,Feo2016}. However, only BSSN+PPM appears to be in the convergent regime at these resolutions, while BSSN+MP5 and CCZ4+WENO are not. In the CCZ4 case, an explanation for the need of a higher resolution, respect to BSSN, to enter in the convergent regime was already given in ref. \cite{Alic2012}: the Hamiltonian constraint, even if damped, enters in the evolution equations of the CCZ4 system, which are, then, more sensitive to numerical errors. More studies are instead needed to explain while the MP5 reconstruction method, which was found best in a TOV star test in ref. \cite{Mosta2014}, performed so poorly in our BNS simulations. For the two combinations in the convergent regime, it is possible to perform a fit to equation (\ref{eq:conv1}), in order to obtain the merger time at infinite resolution and the convergence order (only in the case of WENO, because we have too few simulations in the convergent regime for PPM respect to the fitted parameters. Therefore, in the PPM case, a second order convergence was assumed in order to estimate the real merger time). The results are $p = 1.96 \pm 0.14$, compatible with second order convergence, and $t_{merger}^{dx=0} = 10.39 \pm 0.03$ in the WENO case, $t_{merger}^{dx=0} = 10.55 \pm 0.20$ in the PPM case. The two results for the merger time at infinite resolution are, therefore, compatible within the statistical error. 

Second order convergence is indeed expected from the adopted numerical methods: the time evolution is done with a fourth order Runge-Kutta method, the curvature evolution with fourth-order finite difference, the reconstruction with fifth order (WENO and MP5) or third order (PPM) methods, but the limitation to second order convergence is given by approximating the cells volume averages with their values at cells centres (see sec. \ref{sec:hydro}). Numerical GRHD codes able to go beyond second order convergence have been developed recently \cite{Radice2014}, even if it is proven difficult to get a convergence order higher than two in practical simulations with realistic EOSs \cite{Bernuzzi2016}.

\begin{figure}
\begin{centering}
  \includegraphics[]{figs/OverviewSly14vs14_r25_4met.pdf}
\end{centering}
\caption{Comparison of the evolution of model SLy1.4vs1.4 at resolution
$dx=\SI{0.25}{\msun}$ , using different reconstruction methods for the hydrodynamics
equations and different evolution scheme for the gravitational sector. Quantities
are aligned in time at their respective (different) merger times.
The upper panel shows the Fourier-transform (in arbitrary units) of the 
GW signal in the $22$-mode of the whole signal from 
$t_{\mathrm{merger}}-9$ ms to $t_{\mathrm{merger}}+11$ ms, where a
Blackman-windowing function has been applied.  The second panel shows 
the total GW  luminosity (energy flux). Finally, the bottom panel 
shows the envelop of the gravitational wave amplitudes and its real 
part, multiplied by the distance to the observer.
Please note that the evolutions are all very similar to each other, and they differ 
mainly for the merger time, but not that much in the amount of total energy carried 
away by GW in this stage, and also not in the damping time
of the final excitation of the merger remnant, except for the MP5 case (see text).}
%%%%\todo{FL: We use CCZ4 throughout the paper, we should also here.}
\label{fig:meth_obs}
\end{figure}

Figure \ref{fig:meth_obs} shows the impact of the numerical method choice on the GW signal, its spectrum and the radiated energy flux. All these observables do not change adopting different numerical techniques, confirming the robustness of HRSC finite-volume GRHD codes. The only difference is a faster dissipation of the post-merger GW signal using the MP5 reconstruction methods. As noted before, the MP5 implementation in \codename{The Einstein Toolkit} would benefit from some more tests, in order to understand why it shows poor accuracy in real production applications.

\subsection{Analysing convergence with different observables}
Since the merger time can give only a global indication about the convergence order in the inspiral phase, it is useful to perform, on different observables, also a three-levels convergence test (eq. \ref{eq:3pconv}) at each discrete point in time of the numerical evolution, before the merger.

This test is performed on the same equal mass model with SLy EOS and mass \SI{1.4}{\msun} for each star, using the BSSN formulation of Einstein's equations and the WENO reconstruction method. Three triads of resolutions are used: the \textit{High} one $dx = \rnd{0.125,0.1875,0.25} M_{\odot}$, the \textit{Medium} one $dx = \rnd{0.1875,0.25,0.3125} M_{\odot}$, and the \textit{Low} one $dx = \rnd{0.25,0.3125,0.375} M_{\odot}$. Since the convergence order can potentially change from one triad to another, it is mandatory to verify that it remains more or less the same, in order to be able to claim a certain convergence order for a numerical code \cite{Bernuzzi2016}.

\begin{figure}
\begin{centering}
  \includegraphics[width=\hsize]{figs/png/convergence_phi_thesis.png}\\
\end{centering}
\vspace{-2mm}
\caption{Accumulated gravitational wave phase for simulations with different resolutions, of the same model SLy1.4vs1.4 (left panel). The coloured dots mark the merger time. The right panel, instead, shows the convergence order of the GW phase for three resolution triads (defined in the text). The vertical lines mark the time of contact of the crusts for the lowest resolution simulation of each triad.}
\label{fig:phi_conv}
\end{figure}
In figure \ref{fig:phi_conv}, is reported the accumulated gravitational wave phase $\phi = arctan\frac{h_{\times}}{h_+}$, computed from zero retarded time, at the different resolutions (left panel), and the computed convergence order of the three resolution triads (right panel). The convergence order stays around two for the Low and Medium triad, from \SI{2}{ms} (excluding the initial spurious radiation), until when the neutron stars crust, in the poorest of the three resolutions, come into contact (vertical lines). After the contact, a binary is in a totally different physical state than the ones still inspiralling, therefore no convergence is expected. This is consistent with what is reported also in ref. \cite{Radice2014}. The contact times where extracted from the two dimensional density slices, taking the point in time when the fourth polytropic pieces, representing the inner crust ($\rho_3 = \SI{2.6e12}{g/cm^3}$, see table \ref{tab:low_eos}), come into contact for the first time. A lower convergence order is, instead, observed in the High triad. This is probably due to the presence of some subdominant error source. The dominant error scales with second order convergence, as expected; when the dominant errors becomes comparable with the subdominant one, increasing the resolution, the subdominant error scaling will impact the measured convergence order. One possible source of this subdominat error are the shocks formed at the stars surface, when they start to move inside the zero-velocity numerical atmosphere. These shocks release thermal energy and eject low-density matter from the stars. In the recent ref. \cite{Kastaun2016}, in order to address this problem, the EOS thermal component was switched off for the most part of the inspiral phase, avoiding an extra unphysical thermal pressure at the star surface, but not guaranteeing thermodynamic consistency. A better solution would be to implement more advanced approximated Riemann solvers, like the HLLC solver \cite{White2015}, which are able to distinguish also contact discontinuities, not considered in the simple two-waves structure of HLLE (see sec. \ref{sec:HLLE}).

\begin{figure}
\begin{centering}
  \includegraphics[width=\hsize]{figs/png/convergence_EJ_thesis.png}\\
\end{centering}
\vspace{-2mm}
\caption{Reduced energy over reduced angular momentum curves with different resolutions, of the same model SLy1.4vs1.4 (left panel). The coloured dots mark the merger time. The right panel, instead, shows the convergence order of the $E_r(J_r)$ curves for three resolution triads (defined in the text). The vertical lines mark the reduced angular momentum at contact of the crusts for the lowest resolution simulation of each triad.}
\label{fig:EJ_conv}
\end{figure}

The same picture comes also from the convergence of the gauge-invariant $E_r(J_r)$ curves (see sec. \ref{sec:e} for details on their computation), plotted in figure \ref{fig:EJ_conv}. The Low and Medium triads are around second order convergent, while the High one in only firs-order convergent. Again, the convergence is lost for $J_r$ lower than the angular momentum which characterizes the moment in which the stars crust come into contact in the lowest resolution simulation. The average convergence order, from \SI{2}{ms} to the contact point, are reported in table \ref{tab:conv}.

\begin{table}
\begin{tabular}{|c|c|c|c|}
\hline
Observable &dx = (0.25, 0.3125, 0.375)& dx = (0.1875, 0.25, 0.3125)& dx = (0.125, 0.1875, 0.25)\\
\hline
$\phi_{GW}$ & 2.13 & 1.94 & 1.46\\
$E_r(J_r)$ & 2.24 & 1.86 & 1.11\\
\hline
\end{tabular}
\caption{Average convergence order, computed form \SI{2}{ms} to the neutron star crusts contact, for three different triads of resolutions (see text) and two different observables, the accumulated gravitational wave phase and the gauge-invariant $E_r(J_r)$ curves.}
\label{tab:conv}
\end{table}



