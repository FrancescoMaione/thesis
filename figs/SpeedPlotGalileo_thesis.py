import numpy as np
import matplotlib.pyplot as plt


fig = plt.figure(figsize=(9,7))
#ax1 = fig.add_subplot(2,1,1)
#ax2 = fig.add_subplot(2,1,2)
#bx1 = fig.add_subplot(2,2,2)
#bx2 = fig.add_subplot(2,2,4)
ax1 = fig.add_axes([0.1 ,0.5 ,0.85 ,0.45])
ax2 = fig.add_axes([0.1 ,0.1 ,0.85 ,0.40])

###################### GALILEO ###################################
dx,NP,nt,speed=np.loadtxt('DataBNS_galileo.txt',unpack=True)
idx09,=np.where((dx==0.09375)& (nt ==8)) 
idx12,=np.where((dx==0.125)& (nt ==8)) 
idx19,=np.where((dx==0.1875)& (nt ==8)) 
idx25,=np.where((dx==0.25) & (nt ==8)) 
idx38,=np.where((dx==0.375)& (nt ==8))
idx50,=np.where((dx==0.5)  & (nt ==8))
idx75,=np.where((dx==0.75) & (nt ==8))

idx75_16,=np.where((dx==0.75) & (nt ==16))
idx75_04,=np.where((dx==0.75) & (nt ==4))
idx75_02,=np.where((dx==0.75) & (nt ==2))
idx75_01,=np.where((dx==0.75) & (nt ==1))

idx75A,=np.where((dx==0.75))
ax1.loglog(NP,speed,'kd')
ax1.loglog(NP[idx75_01],speed[idx75_01],'s',c='brown')
ax1.loglog(NP[idx75_02],speed[idx75_02],'s',c='yellow')
ax1.loglog(NP[idx75_04],speed[idx75_04],'s',c='grey')
ax1.loglog(NP[idx75_16],speed[idx75_16],'s',c='orange')
ax1.loglog(NP[idx75],speed[idx75],'ko-')
ax1.loglog(NP[idx50],speed[idx50],'bo-')
ax1.loglog(NP[idx38],speed[idx38],'go-')
ax1.loglog(NP[idx25],speed[idx25],'ro-')
ax1.loglog(NP[idx19],speed[idx19],'mo-')
ax1.loglog(NP[idx12],speed[idx12],'co-')
ax1.loglog(NP[idx09],speed[idx09],'ro-')
ax1.loglog(NP[idx75],NP[idx75]/NP[idx75[0]]*speed[idx75[0]],'k-.')
ax1.loglog(NP[idx50],NP[idx50]/NP[idx50[0]]*speed[idx50[0]],'k-.')
ax1.loglog(NP[idx38],NP[idx38]/NP[idx38[0]]*speed[idx38[0]],'k-.')
ax1.loglog(NP[idx25],NP[idx25]/NP[idx25[0]]*speed[idx25[0]],'k-.')
ax1.loglog(NP[idx19],NP[idx19]/NP[idx19[0]]*speed[idx19[0]],'k-.')
ax1.loglog(NP[idx12],NP[idx12]/NP[idx12[0]]*speed[idx12[0]],'k-.')
ax1.loglog(NP[idx09],NP[idx09]/NP[idx09[0]]*speed[idx09[0]],'k-.')
ax1.plot([16,16]  ,[1,10000],'k:')
ax1.plot([32,32]  ,[1,10000],'k:')
ax1.plot([64,64]  ,[1,10000],'k:')
ax1.plot([128,128],[1,10000],'k:')
ax1.plot([256,256],[1,10000],'k:')
ax1.plot([512,512],[1,10000],'k:')
ax1.plot([1024,1024],[1,10000],'k:')
ax1.plot([2048,2048],[1,10000],'k:')
ax1.text(  16,1000,'#nodes=1\n#cores=16',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text(  32,1000,'#nodes=2\n#cores=32',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text(  64,1000,'#nodes=4\n#cores=64',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text( 128,1000,'#nodes=8\n#cores=128',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text( 256,1000,'#nodes=16\n#cores=256',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text( 512,1000,'#nodes=32\n#cores=512',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text(1024,1000,'#nodes=64\n#cores=1024',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text(2048,1000,'#nodes=128\n#cores=2048',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)
ax1.text(4096,1000,'#nodes=256\n#cores=4096',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='center',fontsize=8)

ax1.text(16,400,'$dx=0.75$')
ax1.text(16, 50,'$dx=0.50$')
ax1.text(16, 20,'$dx=0.375$')
ax1.text(40, 12,'$dx=0.25$')
ax1.text(100, 9,'$dx=0.1875$')
ax1.text(300, 7,'$dx=0.125$')
ax1.text(1000,7,'$dx=0.09375$')

#ax2.semilogx(NP[idx75A],((NP[idx75A]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75A]))**(-1),'kd')
ax2.semilogx(NP[idx75_16],((NP[idx75_16]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_16]))**(-1),'s',c='orange')
ax2.semilogx(NP[idx75_01],((NP[idx75_01]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_01]))**(-1),'s',color='brown')
ax2.semilogx(NP[idx75_02],((NP[idx75_02]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_02]))**(-1),'s',color='yellow')
ax2.semilogx(NP[idx75_04],((NP[idx75_04]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_04]))**(-1),'s',color='grey')

ax2.semilogx(NP[idx75_01],((NP[idx75_16]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_01]))**(-1),'--',c='brown')
ax2.semilogx(NP[idx75_02],((NP[idx75_16]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_02]))**(-1),'y--')
ax2.semilogx(NP[idx75_04],((NP[idx75_16]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_04]))**(-1),'--',c='grey')
ax2.semilogx(NP[idx75_16],((NP[idx75_16]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75_16]))**(-1),'--',c='orange')

lineorange = plt.Line2D(range(1),range(1),color="white",marker='s',markerfacecolor='orange')
linebrown = plt.Line2D(range(1),range(1),color="white",marker='s',markerfacecolor='brown')
lineyellow = plt.Line2D(range(1),range(1),color="white",marker='s',markerfacecolor='yellow')
linegrey = plt.Line2D(range(1),range(1),color="white",marker='s',markerfacecolor='grey')

ax2.legend((linebrown,lineyellow,linegrey,lineorange),('NT=2','NT=4','NT=8','NT=16'),numpoints=1,loc=0)

ax2.semilogx(NP[idx75],((NP[idx75]/NP[idx75[0]])*(speed[idx75][0]/speed[idx75]))**(-1),'ko-')
ax2.semilogx(NP[idx50],((NP[idx50]/NP[idx50[0]])*(speed[idx50][0]/speed[idx50]))**(-1),'bo-')
ax2.semilogx(NP[idx38],((NP[idx38]/NP[idx38[0]])*(speed[idx38][0]/speed[idx38]))**(-1),'go-')
ax2.semilogx(NP[idx25],((NP[idx25]/NP[idx25[0]])*(speed[idx25][0]/speed[idx25]))**(-1),'ro-')
ax2.semilogx(NP[idx19],((NP[idx19]/NP[idx19[0]])*(speed[idx19][0]/speed[idx19]))**(-1),'mo-')
ax2.semilogx(NP[idx12],((NP[idx12]/NP[idx12[0]])*(speed[idx12][0]/speed[idx12]))**(-1),'co-')
ax2.semilogx(NP[idx09],((NP[idx09]/NP[idx09[0]])*(speed[idx09][0]/speed[idx09]))**(-1),'ro-')


#ax2.text(75,0.38 ,'Np=1,Nt=16',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='left',fontsize=8)
#ax2.text(75,0.44,'Np=16,Nt=1',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='left',fontsize=8)
#ax2.text(75,0.50,'Np=8,Nt=2',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='left',fontsize=8)
#ax2.text(75,0.56,'Np=4,Nt=4',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='left',fontsize=8)
#ax2.text(75,0.62,'Np=2,Nt=8',bbox={'facecolor':'w', 'alpha':1, 'pad':5},horizontalalignment='left',fontsize=8)


ax2.grid(axis='y')
#ax2.plot([16,512],[1,1],'k')
ax2.plot([16,16]  ,[0.3,1.2],'k:')
ax2.plot([32,32]  ,[0.3,1.2],'k:')
ax2.plot([64,64]  ,[0.3,1.2],'k:')
ax2.plot([128,128],[0.3,1.2],'k:')
ax2.plot([256,256],[0.3,1.2],'k:')
ax2.plot([512,512],[0.3,1.2],'k:')
ax2.plot([1024,1024],[0.3,1.2],'k:')
ax2.plot([2048,2048],[0.3,1.2],'k:')

ax1.set_xlim(12,5200)
ax2.set_xlim(12,5200)

ax1.set_ylim(6,2000)
ax2.set_ylim(0.34,1.1)




ax1.set_title('CINECA - GALILEO system (scaling 1 MPI process per socket)')
ax1.set_ylabel('speed (CU/hour)')
#ax1.set_xlabel('# of cores')


ax2.set_ylabel('efficiency')
ax2.set_xlabel('# of cores')


fig.savefig('SpeedPlotGalileo_thesis.png')
fig.savefig('SpeedPlotGalileo_thesis.pdf')
