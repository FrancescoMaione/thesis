\label{sec:guide}
In this last appendix, are listed some simple instructions to perform general-relativistic binary neutron star mergers simulations, using only publicly available codes. Some of the codes and parameter files needed to reproduce this thesis results, should be downloaded from the Parma relativity group public Subversion repository:
\begin{lstlisting}
$ svn --username=anonymous checkout \
	https://einstein.pr.infn.it/svn/numrel/pub/
password: anon
\end{lstlisting}
In order to run a BNS merger simulation, one needs, first, to generate appropriate initial data (crf. sec. \ref{sec:initial_data}). This can be done using the public \codename{LORENE} library. It can be downloaded freely from its cvs repository, using the following commands in a bash shell:
\begin{lstlisting}[language=bash]
  $ cvs -d :pserver:anonymous@octane.obspm.fr:/cvsroot login 
  password: anonymous
  $ cvs -z5 -d :pserver:anonymous@octane.obspm.fr:\
  	/cvsroot checkout Lorene
\end{lstlisting}
Next, one needs to compile it, choosing appropriate configuration parameter in the file \emph{local\_settings} (which can be found in the main LORENE directory). Example configurations for the most common OSs can be found in the \emph{Local\_settings\_examples} folder. The code to generate BNS initial configurations, using the conformal thin sandwich approximation, is contained in \emph{Codes/Bin\_star}. To generate the initial data it is necessary to run first the \emph{init\_bin} executable, in order to generate two isolated stars solutions, with the desired EOS, and, then, run \emph{coal}, to compute the proper binary configuration. The output result is contained in a binary file called \emph{resu.d}, which is readable by appropriate routines in \codename{The Einstein Toolkit}, or codes contained in LORENE's \emph{Export} subfolder. Before running the initial data code, its setting should be configured, modifying the files \emph{par\_grid.d} (to set the computational domain and its resolution), \emph{par\_eos.d} (to choose the EOS used, either a polytropic, one of the tabulated EOSs distributed with LORENE, or a user-defined table, in LORENE's or CompOSE format) and \emph{parcoal.d} (to select, among other things, the initial distance between the stars centres and the stars baryonic mass). An example of LORENE's configuration files for the initial data of the simulations presented in this thesis can be found in the Parma relativity group public repository, under the \emph{BNS\_2015\_work/InitialData} folder. In particular, the EOS was set as a table file, generated from the piecewise polytropic parametrization of sec. \ref{sec:eos}. Examples of EOS table files in LORENE's format can be also found in the Parma group public repository, under the \emph{BNS\_2015\_work/Eos\_tables} folder.

To evolve the initial data, one needs to download, compile and run \codename{The Einstein Toolkit}. For obtaining the current last version (May 2016), it is useful to download and run the script \codename{GetComponents}. To be able to reproduce this thesis simulations, one should also download the Parma group minimal thornlist (the list of modules to download and compile) for BNS merger simulations, which includes also our modified version of some Cactus thorns (in particular, \codename{EOS\_Omni} and \codename{Meudon\_Bin\_NS}, in order to be able to use piecewise polytropic EOSs) and our analysis modules (\codename{GRHydro\_Analysis}).
\begin{lstlisting}[language=bash]
  $ curl -kLO https://raw.githubusercontent.com\
  	/gridaphobe/CRL/ET_2016_05/GetComponents
  $ chmod a+x GetComponents
  $ ./GetComponents --parallel \
  	http://www.fis.unipr.it/gravity/Research/BNS2015.th
\end{lstlisting}
Then, it's important to set the proper configuration files. Four configuration files, found in the subdirectory \emph{simfactory/mdb} of the main Cactus installation directory, are important for \codename{simfactory}, the Python code responsible for the installation process and the simulations running: 
\begin{itemize}
\item The optionlist (\textit{simfactory/mdb/optionlists/*.cfg}), containing the compilation flags;
\item The machine file (\textit{simfactory/mdb/machines/*.ini}, containing information about the machine hardware;
\item The submitscript (\textit{simfactory/mdb/submitscripts/*.sub)}, containing the parameters to pass to the batch queuing system, when running on a shared cluster;
\item The runscript (\textit{simfactory/mdb/runscripts/*.run}), containing the command to run the setting the needed environmental variables. 
\end{itemize}
The standard installation of The Einstein Toolkit already contains several examples of those files, for most OSs, Linux distributions, and many clusters around the world, including Galileo, Queenbee, and Mike, where this thesis simulations were performed.

After having found and eventually modified the correct configuration files, one can compile all the toolkit with the simple command
\begin{lstlisting}[language=bash]
  $ simfactory/bin/sim build CONFIGURATION_NAME\
  	 --thornlist THORNLIST.th --optionlist OPTIONLIST.cfg \
  	 --runscript RUNSCRIPT.run --submitscript SUBMITSCRIPT.sub \
  	 --machine MACHINE_NAME
\end{lstlisting}
where the upper case words should be substituted with the appropriate names or file paths. Multiple configurations, with different configurations names, and different parameters in the configuration files, can be created and compiled.
After the compilation (which can require some minutes to several ours, depending on the platform), one still needs to set up a parameter file, before being able to run the desired simulation. A parameter file contains the list of the used module (called \textit{active thorns}) and sets their parameters. For example, one should set the grid structure, through the parameters of the \codename{CarpetRegrid2} thorn, or the numerical methods desired for the hydrodynamics, with the \codename{GRHydro} thorn parameters, or the variables one wants to output in 1D, 2D and 3D files. Other important things to set are the stars EOS (with the \codename{EOS\_Omni} thorn) and the path to the initial LORENE data file (the \textit{resu.d} produced in the previous step). All the parameter files for this thesis simulations can be downloaded from the Parma group Subversion repository, in folder \emph{BNS\_2015\_work/WorkBNS}.

To submit a simulation to the queuing system, one just needs to run the following command, from the main Cactus installation folder:
\begin{lstlisting}[language=bash]
  $ simfactory/bin/sim submit SIMULATION_NAME \
  	--procs NP --num-threads NT --num-smt NS \
  	--parfile PARFILE.par --configuration CONFIGURATION_NAME\
  	--walltime hh:mm:ss
\end{lstlisting} 
where NP is the total number of threads, NT is the number of threads for each MPI process, NS is the number of threads for each core, which can be greater than one in machines where hyperthreading is enabled, and walltime is the maximum running time, often imposed by the system administrators. In order to run long simulation, one can restart an interrupted simulation running the same submit command. It will start again from the last saved checkpoint file (checkpoint saving frequency can be set in the parameter file). Instead, to run a simulation directly on the machine one is logged in, the command \textit{create-run} (for the first run), or simply \textit{run} (for the subsequent restarts) can be used instead of \textit{submit}. In this case, the option \textit{walltime} will be ignored, using the one set in the parameter file (or its default value).

To analyse the results, several tools have been developed by different groups, listed on the webpage \emph{https://docs.einsteintoolkit.org/et-docs/Analysis\_and\_post-processing}, but there is no single official Einstein Toolkit post-processing tool yet. I will illustrate some simple use-cases of the Parma group \codename{PyCactus} code, which can be downloaded from its Git repository
\begin{lstlisting}[language=bash]
  $ git clone https://bitbucket.org/knarrff/pycactus.git
\end{lstlisting}
\codename{PyCactus} is a Python code which heavily uses the \textit{numpy}, \textit{scipy} and \textit{matplotlib} scientific libraries. It can be efficiently used in an interactive way with the \textit{ipython} program. Just to give an example of its capabilities, I will illustrate the steps needed to extract and plot the gravitational waves signal from a Cactus simulation.

First, one needs to load the \codename{PyCactus} modules:
\begin{lstlisting}[language=python]
from UtilityUnits import *
import CarpetASCIII as asc
import CarpetH5 as H5
import UtilityAnalysis as Analysis
import CarpetSimulationData  sim
from matplotlib import pyplot as plt
import numpy as np
\end{lstlisting}
To load all the scalar output from a simulation, and save it in an hdf5 file, one can use the following command (which, if the hdf5 file already exists, just reads its content):
\begin{lstlisting}[language=python]
d = sim.ReadAllScalarsSimulation(sim_name,basedir,folderH5)  
\end{lstlisting}
Form there, to get the gravitational wave signal, extrapolated with the second order perturbative formula and integrated with our filtering procedure (see sec. \ref{sec:extraction}), and to plot its $h_+$ component, one needs simply to use the following commands:
\begin{lstlisting}[language=python]
  psi_inf=Analysis.second_order_extrapolation(d['PSI4'][r0]['l2m2'],
  	int(r0),d['M0'],2,2,2,'filter',f0=f0)
  h22  = Analysis.NIntegratePsi4_filter(psi_inf,f0,Nint=2,r=0,M=d['M0'])
  plt.plot(d['time']*CU_to_ms,np.real(h22))
\end{lstlisting}
